<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Для покупки в интернет магазине 3кон, необходимо всего 3 действия.");
$APPLICATION->SetPageProperty("keywords", "Как купить мебель в интернет магазине 3кон");
$APPLICATION->SetPageProperty("title", "Как купить в интернет магазине 3кон");
$APPLICATION->SetTitle("Как купить в интернет магазине 3кон");
?>

    <nav class="menu_content"><a href="/">Главная</a> » Как купить</nav>
    <section class="how_buy">
        <h1>Как купить</h1>
        <div class="clearfix">
            <article class="how_buy_block">
                <div class="img">
                    <img src="img/howbuy_img.png" width="200" height="211" alt=""/>
                </div>
                <h2 class="h2">1. Заказ мебели по телефону.</h2>
                <p>
                    Заказ мебели по телефону <a class="phone_num" href="tel:84957964174">8 (495) 796-41-74</a> быстро и удобно. Многоканальный телефон всегда дает возможность быть с нами на связи в рабочее время нашего офиса.
                </p>
                <p>
                    А с опытными менеджерами <a href="https://www.3kon.ru/">3KON.RU</a> процедура оформления заказа по телефону происходит быстро.
                </p>
            </article>
            <!-- end .how_buy_block -->
            <article class="how_buy_block">
                <div class="img">
                    <img src="img/howbuy_img2.png" width="200" height="215" alt=""/>
                </div>
                <h2 class="h2">2. Заказ мебели через корзину на сайте (онлайн-заказ).</h2>
                <p>
                    Заказать мебель через <a href="/korzina.html">корзину</a> очень удобный способ, который дает возможность сделать заказ прямо на сайте в любое время дня и ночи.
                </p>
                <p>
                    Для этого просто добавьте нужный товар в корзину, кликнув на значок корзины рядом
                    с ценой товара и нужная Вам товарная позиция сразу попадет в корзину заказа.
                </p>
                <p>
                    После того, как Вы будете готовы оформить заказ кликайте на значок «Оформить заказ».
                </p>
            </article>
            <!-- end .how_buy_block -->
        </div>
        <!-- end .clearfix -->
        <article class="how_buy_bottom">
            <div class="img">
                <img src="img/howbuy_img3.png" width="500" height="204" alt=""/>
            </div>
            <div class="nofloat">
                <h2 class="h2">Обработка Вашего заказа.</h2>
                <p>
                    После получения Вашего заказа по телефону или через корзину - менеджер сразу передает его в работу.
                    Обработка заказов происходит в рабочее время, поэтому, если Вы сделали заказ через корзину
                    в нерабочее время, то мы его обработаем сразу, как только будет возможно и перезвоним Вам по указанному в заказе телефону для уточнения всех деталей.
                </p>
                <a class="btn btn_order" href="/oplata.php"><span>Оплата заказа</span></a>
            </div>
            <!-- end .nofloat -->
        </article>
        <!-- end .how_buy_bottom -->
    </section>
    <!-- end .how_buy -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult["MENU"])):?>
<nav>
    <ul class="catalog-menu">
    <?for($i = 0; $i < 9; $i++):?>
        <li class="catalog-menu__item">
            <a class="catalog-menu__link" href="<?=$arResult["MENU"][$i][1]?>"><?=$arResult["MENU"][$i][0]?></a>
        </li>
    <?endfor;?>
    </ul>
    <ul class="catalog-menu m-catalog-menu_last">
        <?for($i = 9; $i <= count($arResult["MENU"]); $i++):?>
            <li class="catalog-menu__item">
                <a class="catalog-menu__link" href="<?=$arResult["MENU"][$i][1]?>"><?=$arResult["MENU"][$i][0]?></a>
            </li>
        <?endfor;?>
    </ul>
</nav>
<?endif?>
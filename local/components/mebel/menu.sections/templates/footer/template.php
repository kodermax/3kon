<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult["MENU"])):?>
<nav>
    <ul class="links-list m-links-list_two-columns">
    <?foreach($arResult["MENU"] as $arItem):?>
        <li class="links-list__item">
            <a class="links-list__link" href="<?=$arItem[1]?>"><?=$arItem[0]?></a>
        </li>
    <?endforeach;?>
    </ul>
</nav>
<?endif?>
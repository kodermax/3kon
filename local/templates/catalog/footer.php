<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
</div>
</div>
<footer class="footer">
    <div class="footer__bg"></div>
    <div class="layout">
        <div class="footer__inner">
            <table class="footer__sections">
                <tr>
                    <td class="footer__sections-item" colspan="2">
                        <div class="footer__logo">
                            <a href="/">
                                <img src="/images/logo_small.png" width="133" height="15" alt="интернет-магазин мебели" title="интернет-магазин мебели">
                            </a>
                        </div>
                        <div class="footer__copy-line" itemprop="name">&copy;&nbsp;Интернет-магазин мебели &laquo;Мебель таргет&raquo;</div>
                        <div class="footer__copy-line">Телефон в Москве: <div itemprop="telephone" class="big-phone">(495)&nbsp;972-96-79</div></div>
                    </td>
                    <td class="footer__sections-item">
                        <nav>


                            <ul class="links-list">
                                <li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/">Главная</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/about/">О магазине</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/to_buyers/">Покупателям</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/to_buyers/">Как заказать</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/payment/">Оплата</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/delivery/">Доставка</a></li>    </ul>

                        </nav>
                    </td>
                    <td class="footer__sections-item" colspan="2">
                        <nav>
                            <ul class="links-list m-links-list_two-columns">
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/banketki-puphy/">Пуфы и банкетки</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/pletenaya-mebel-iz-rotanga/">Плетеная мебель</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/predmety/">Предметы интерьера</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/barnaya-mebel/">Барная мебель</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/veshalki/">Вешалки</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/detskaya-mebel/">Детская мебель</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/komody/">Комоды</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/kresla/">Кресла</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/krovati/">Кровати</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/kuhonnye-ugolki/">Кухонные уголки</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/obuvnicy/">Обувницы</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/ofisnaya-mebel/">Офисная мебель</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/prihozhii/">Прихожии</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/spalni/">Спальни</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/stoly/">Столы</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/stylya-taburetki/">Стулья и табуретки</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/tvtumby/">ТВ-тумбы</a>
                                </li>
                                <li class="links-list__item">
                                    <a class="links-list__link" href="/catalog/shkafy/">Шкафы</a>
                                </li>
                            </ul>

                        </nav>
                    </td>
                </tr>
            </table>
            <div class="footer__basket">
                <div class="footer__basket-title"><a href="/personal/cart/">Корзина</a></div>
                <div class="footer__basket-data">
                    В корзине пока ничего нет. <a href="/catalog/">Перейти в каталог</a>.
                </div>
            </div>
        </div>
    </div>
</footer>
</section>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter19358860 = new Ya.Metrika({
                    id:19358860,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/19358860" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
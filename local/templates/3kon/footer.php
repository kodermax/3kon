</td>

</tr>
<tr>
        <?$APPLICATION->ShowViewContent('content2');?>
        <?if ($curPage === SITE_DIR."index.php"):?>
            <td class="content2"><h2>Немного о стеклянных столах</h2>
                <p>Несмотря на то, что в России <strong>стеклянные столы</strong> появились относительно недавно, они уже успели завоевать большую популярность среди покупателей, чему способствовало во многом их практичность. Ведь в конструкции этих предметов мебели достигнута превосходная гармония высокой эстетики, надёжности и комфорта. <br>Отличная внешняя сочетаемость стекла с различными материалами: металлом, ротангом, пластиком или деревом позволяет изготавливать каркасы изделий практически из чего угодно, а это в свою очередь делает возможным просто и быстро подобрать стол под любой стиль интерьера, будь то классика или хайтек. В частности, журнальный <strong>стеклянный стол</strong> станет отличным дополнением интерьера любой гостиницы и спальни в независимости от их оформления. А благодаря внешней лёгкости стеклянные столы отлично вписываются во внутреннее убранство небольшой кухни или комнаты, совершенно не загромождая ценное пространство. <br>Стеклянные столы не знают что такое грязь и царапины. С помощью тряпки такой журнальный, обеденный или <strong>компьютерный стол</strong> легко очищается от разлитого масла, клея, пепла, детских фломастеров, остатков пищевых продуктов и других загрязнений, устойчивы они и к воздействию моющих средств. <br>Несмотря на невесомый и хрупкий внешний вид, стеклянные столы обладают высокой прочностью. Разбить крепкое закалённое стекло не так просто как кажется на первый взгляд. Однако, если по какой-то причине столешница всё же подвергнется очень сильному удару, она разлетится на безопасные осколки, которыми невозможно повредить руки. Кроме того, используемое в конструкции стекло защищено специальным слоем, который заметно повышает устойчивость изделия к механическим повреждениям. Даже при интенсивной эксплуатации, например, использования стеклянного стола для приготовления пищи на кухне, столешница всегда будет иметь идеально гладкую поверхность и сверкать как новая долгое время. Безопасен стол и с точки зрения экологии. Применяемые для его изготовления натуральные материалы никогда не вызовут проблем со здоровьем.&nbsp; <br><a href="#">Наш Интернет-магазин мебели</a> предлагает стеклянные столы разных типов: прямоугольные, круглые или овальные, а в зависимости от типа станины можно выбрать раздвижной, раскладной, стационарный либо стол-трансформер. Мы находимся в г. Москва</p>
                </td>
        <?endif;?>
</tr>


</table>
</div></td>
</tr>
<tr>
    <TD height="100%" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" valign="top" colspan="3"> <div align="center"><img src="/images/red_line.jpg" width="100%" height="6px"></div></td>
            </tr><tr>
                <td style="width:20%;text-align:left;padding:10px;"></td>
                <td style="width:60%;height:26px;padding-top:20px;" align="left" valign="middle">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                        "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    ),
                        false
                    );
                    ?>
                </td>
            </tr>
        </table></TD>

</tr>
</table>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22658326 = new Ya.Metrika({
                    id:22658326,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22658326" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Сleversite chat button -->
        <script type='text/javascript'>
                (function() {
                        var s = document.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.charset = 'utf-8';
                        s.src = '//cleversite.ru/cleversite/widget_new.php?supercode=1&referer_main='+encodeURIComponent(document.referrer)+'&clid=14419RsBky&siteNew=19966';
                        var ss = document.getElementsByTagName('script')[0];
                        ss.parentNode.insertBefore(s, ss);
                })();
        </script>
<!-- / End of Сleversite chat button -->

</body>
</html>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div style="max-width: 800px;">

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="jot-row <?if($key % 2 == 0):?>jot-row-alt<?endif;?>">
        <div class="jot-comment">
            <div class="jot-user">
                <?=$arItem['PROPERTIES']['USER_NAME']['VALUE']?>
            </div>
            <div class="jot-content">
                <div class="jot-mod">

                </div>

                <span class="jot-subject"><?=$arItem['NAME']?></span><br>
                <span class="jot-poster"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
                <hr>
                <div class="jot-message"><?=$arItem['PREVIEW_TEXT']?></div>
                <div class="jot-extra">
                </div>
            </div>

        </div>

    </div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

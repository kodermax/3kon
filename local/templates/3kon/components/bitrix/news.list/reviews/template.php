<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo "<pre>";print_r($arResult);echo "</pre>";
?>
<div style="padding-left: 0px; padding-top: 6px;">
    <table width="215" cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <tr>
                <td valign="top" height="30" align="left" style="background-repeat: no-repeat; font-size: 10px;background-image: url('/images/help.jpg');" class="style8"><div style="padding-left: 60px; padding-top: 14px;">ОТЗЫВЫ </div></td>
            </tr>
        <tr>
            <td valign="top" align="left">
                <div style="padding-left: 7px; padding-top: 0px; padding-right: 0px;">
                    <table width="208" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td valign="top" align="left" class="style7">
                                <div style="padding-left: 5px; padding-top: 5px; padding-bottom: 15px;">
                                    <?foreach($arResult["ITEMS"] as $arItem):?>
                                    <div style="padding:4px">
                                        <p style="color:#666;font-size:11px;">  <?=$arItem['PROPERTIES']['USER_NAME']['VALUE']?>  (<b><?=$arItem['NAME']?></b>):   (<?=$arItem['DISPLAY_ACTIVE_FROM']?>)  </p>
                                        <h5 style="font-weight:normal;font-size:12px;"><?=$arItem['PREVIEW_TEXT']?></h5>
                                    </div>
                                    <?endforeach;?>
                                    <br/>
                                    <a href="/otzyvy.html">Все отзывы</a>
                                    </div>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

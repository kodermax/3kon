<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h3 class="new">Новинки</h3>
<div class="new-area">
	<?foreach($arResult["ROWS"] as $arItems):?>
		<?foreach($arItems as $arElement):?>
			<?if(is_array($arElement)):?>
					<div class="catalog_row_index">
						<div class="catalog_row_index_longdescription">
							<div style="text-align: center;"><?=$arElement['NAME']?></div></div>
						<a href="<?=$arElement['DETAIL_PAGE_URL']?>"><img src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" title="<?=$arElement['NAME']?>" alt="<?=$arElement['NAME']?>" width="200" height="200"></a>
						<div class="catalog_row_index_description">
							<a href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['NAME']?></a>
						</div>
					</div>
				<?endif;?>
			<?endforeach;?>
	<?endforeach;?>
</div>

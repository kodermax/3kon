<?
global $APPLICATION;
//$APPLICATION->AddHeadScript("/js/jquery-2.1.4.min.js");
$APPLICATION->AddHeadScript("/js/chosen.jquery.min.js");
$APPLICATION->AddHeadScript("/js/highslide-full.min.js");
$APPLICATION->AddHeadScript("/js/ImageSelect/ImageSelect.jquery.js");
$APPLICATION->SetAdditionalCSS("/js/highslide.css");
$APPLICATION->SetAdditionalCSS("/css/chosen.min.css");
$APPLICATION->SetAdditionalCSS("/js/ImageSelect/ImageSelect.css");
?>
<script type="text/javascript">
// override Highslide settings here
// instead of editing the highslide.js file
    hs.graphicsDir = '/js/graphics/';
</script>

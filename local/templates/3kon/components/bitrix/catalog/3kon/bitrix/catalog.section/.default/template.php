<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<table class="catalog">
    <tbody>
        <tr valign="top">
            <td align="center" class="catalog_header2">Артикул </td>
            <td align="center" class="catalog_header">Наименование <a href="?sort=name&order=asc">^</a>&nbsp;<a href="?sort=name&order=desc">v</a></td>
            <td align="center" class="catalog_header2">Ед.&nbsp;изм.</td>
            <td align="center" class="catalog_header">Цена <a href="?sort=price&order=asc">^</a>&nbsp;<a href="?sort=price&order=desc">v</a></td>
        </tr>
        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
                <tr valign="middle" style ="background-color:<?if($key % 2 == 1):?>#f7f7f9 <?else:?>#ffffff<?endif;?>">
                    <td class="catalog_article"><?=$arItem['PROPERTIES']['ARTICLE']['VALUE']?></td>
                    <td align="center">
                        <table width="100%"><tbody><tr valign="top">
                                <td width="14%" class="catalog_img">
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" title="<?=$arItem['NAME']?>" alt="<?=$arItem['NAME']?>" width="80" height="80"></a>
                                </td>
                                <td width="1%" nowrap=""></td>
                                <td width="85%" class="catalog_title">
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><b> <?=$arItem["NAME"]?> </b></a><p>
                                    </p><div class="catalog_announce"><?=$arItem['PREVIEW_TEXT']?></div>
                                </td>
                            </tr></tbody></table></td>
                    <td class="catalog_value">шт.</td>
                    <td class="catalog_price"><?=$arItem["MIN_PRICE"]["PRINT_VALUE"]?></td>
                </tr>
            <?endforeach;?>
    </tbody>
</table>
<?
    if ($arParams["DISPLAY_BOTTOM_PAGER"])
    {
    ?><? echo $arResult["NAV_STRING"]; ?><?
}
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID.'_pict',
    'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
    'STICKER_ID' => $strMainID.'_stricker',
    'BIG_SLIDER_ID' => $strMainID.'_big_slider',
    'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
    'SLIDER_LIST' => $strMainID.'_slider_list',
    'SLIDER_LEFT' => $strMainID.'_slider_left',
    'SLIDER_RIGHT' => $strMainID.'_slider_right',
    'OLD_PRICE' => $strMainID.'_old_price',
    'PRICE' => $strMainID.'_price',
    'DISCOUNT_PRICE' => $strMainID.'_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
    'QUANTITY' => $strMainID.'_quantity',
    'QUANTITY_DOWN' => $strMainID.'_quant_down',
    'QUANTITY_UP' => $strMainID.'_quant_up',
    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
    'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
    'BUY_LINK' => $strMainID.'_buy_link',
    'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
    'COMPARE_LINK' => $strMainID.'_compare_link',
    'PROP' => $strMainID.'_prop_',
    'PROP_DIV' => $strMainID.'_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
    'OFFER_GROUP' => $strMainID.'_set_group_',
    'ZOOM_DIV' => $strMainID.'_zoom_cont',
    'ZOOM_PICT' => $strMainID.'_zoom_pict',
    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);
?>
<table class="goods" id="<? echo $arItemIDs['ID']; ?>">
    <tbody><tr>
        <td class="goods_img">
            <p class="product-image product-image-zoom">
                <a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="highslide" onclick="return hs.expand(this, {captionId: 'caption24'})">
                    <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" width="267" height="267"></a>
            </p><div class="highslide-caption" id="caption24">
                <?=$arResult['NAME']?>
            </div><p></p>
        </td>
        <td class="goods_desc">
            <div class="goods_name">
                <h1><?=$arResult['NAME']?></h1>
            </div>
            <div class="goods_price">
                Цена: <?=$arResult['MIN_PRICE']['PRINT_VALUE']?>
            </div>
            <div class="goods_article">
                Артикул: <?=$arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE']?>
            </div>

<?if($arResult['IS_SKU']):?>
            <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
                <?
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
                {
                ?>
                <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                <?
                if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                    unset($arResult['PRODUCT_PROPERTIES'][$propID]);
                }?>
            </div>
            <h2 class="product__settings-header">Конфигурация</h2><h2 class="product__settings-header">Конфигурация</h2>
            <div class="sku_div" id="<? echo $arItemIDs['PROP_DIV']; ?>">
               <?if(array_key_exists(1,$arResult['SKU_PROPS'])):?>
                    <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][1]['ID']; ?>_cont">
                    <div class="product__settings-title">Выберите размер</div>
                    <ul id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][1]['ID']; ?>_list">
                        <?foreach($arResult['SKU_PROPS'][1]['VALUES'] as $arValue):?>
                            <li data-treevalue="<?=$arResult['SKU_PROPS'][1]['ID'].'_'.$arValue['ID']; ?>" data-onevalue="<?=$arValue['ID']; ?>">
                                <div class="product__settings-line">
                                    <label class="radio product__settings-item m-product-settings-item">
                                        <input class="radio__field" type="radio" name="skusize" data-price="" value="<?=$arValue["ID"]?>">
                                    <span class="radio__label">
                                        <?=$arValue["NAME"]?>
                                    </span>
                                    </label>
                                </div>
                            </li>
                        <?endforeach;?>
                    </ul>
                </fieldset>
                <?endif;?>
                <?

                $firstElement = current($arResult["SKU_PROPS"][0]["VALUES"]);

                ?>
                <?if(array_key_exists(0, $arResult['SKU_PROPS'])):?>
                    <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][0]['ID']; ?>_cont">
                    <div class="product__settings-title">Выберите цвет</div>
                    <select class="colorSelect">
                        <?foreach($arResult["SKU_PROPS"][0]["VALUES"] as $arValue):?>
                            <option value="<?=$arValue['ID']?>" data-img-src="<?=$arValue["PICT"]["SRC"]?>"><?=$arValue["NAME"]?></option>
                        <?endforeach?>
                    </select>
                </fieldset>
                <?endif;?>
                <?if(count($arResult['SKU_PROPS'][2]["VALUES"]) > 0):?>
                    <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][2]['ID']; ?>_cont">
                        <div class="product__settings-title">Выберите дополнения</div>
                        <div class="product__settings-line">
                            <ul id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][2]['ID']; ?>_list">
                                <?foreach($arResult['SKU_PROPS'][2]["VALUES"] as $arValue):?>
                                    <li data-treevalue="<?=$arResult['SKU_PROPS'][2]['ID'].'_'.$arValue['ID']; ?>" data-onevalue="<?=$arValue['ID']; ?>">
                                        <label class="radio product__settings-item m-product-settings-item_active">
                                            <input class="radio__field" type="radio" name="skuother" value="<?=$arValue['ID']?>" checked="checked" data-price="0">
                                            <span class="radio__label"><?=$arValue['NAME']?></span>
                                        </label>
                                    </li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </fieldset>
                <?endif;?>
            </div>
<?endif;?>
            <div class="goods_addtocart">
                <input id="quantity" type="text" value="1" size="3" onkeypress="return validKey(event)" style="width:20px">
                <span id="addToCartButton" style="cursor:pointer"  data-title="<?=$arResult['NAME']?>"><img style="position:relative; left:0px; top:7px;" src="/images/in_bascet.gif" alt="В корзину"></span>
<div style="margin-top:10px;">
              <?$APPLICATION->IncludeComponent(
	"blackbutterfly:oneclick",
	"",
Array(
"ELEMENT_ID" => $arResult["ID"],
"JQUERY"=>"Y","COLORBOX"=>"Y"),false
);?>
</div>
            </div>
            <!-- div style="margin:20px 0;"></div -->
            <div class="phones_wrap">
                <div class="phones">
                    <p class="phones_text">Консультант подскажет и поможет выбрать мебель:
                    </p>
                    <p class="phones_nums">
                        (495) 796-41-74
                    </p>
                </div>
            </div>

        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="more-views">
                <ul>
                    <?foreach($arResult['MORE_PHOTO'] as $key=>$item):?>
                        <li>
                            <a href="<?=$item['SRC']?>" class="highslide" onclick="return hs.expand(this,{'caption': 'caption<?=$key?>'})">
                                <img src="<?=$item['SRC']?>" width="200" height="200">
                            </a>
                            <div class="highslide-caption" id="caption<?=$key?>">
                               <?=$arResult['NAME']?>
                            </div>

                        </li>
                    <?endforeach;?>
                </ul>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?=$arResult['DETAIL_TEXT']?>
        </td>
    </tr>
   </tbody></table>
<script>
    function ShowWindow(text, width, height) {
        var width;
        var height;
        var text;
        var top;
        var left;

        if (width == "") width = "150";
        if (height == "") height = "100";
        if (text == "") text = "";

        // --------------- Browsers
        isDOM = document.getElementById; //DOM1 browser (MSIE 5+, Netscape 6, Opera 5+)
        isOpera = isOpera5 = window.opera && isDOM;//Opera 5+
        isOpera6 = isOpera && window.print; //Opera 6+
        isOpera7 = isOpera && document.readyState; //Opera 7+
        isMSIE = document.all && document.all.item && !isOpera; //Microsoft Internet Explorer 4+
        isMSIE5 = isDOM && isMSIE; //MSIE 5+
        isNetscape4 = document.layers; //Netscape 4.*
        isMozilla = isDOM && navigator.appName == "Netscape"; //Mozilla & Netscape 6.*
        // -- Get height

        function getHeight() { // Получаем высоту рабочей области браузера
            if (isMSIE || isMozilla || isMSIE) send = document.documentElement.clientHeight;
            if (isOpera6 || isOpera7 || isOpera || isNetscape4) send = window.innerHeight;
            return send;
        }

        // -- Get width

        function getWidth() { // Получаем высоту рабочей области браузера
            if (isMSIE || isMozilla || isMSIE) send = document.documentElement.clientWidth;
            if (isOpera6 || isOpera7 || isOpera || isNetscape4) send = window.innerWidth;
            return send;
        }
        //-----------------------------
        if (document.getElementById('log') == null) {
            document.getElementsByTagName('body')[0].innerHTML += '<div id="log"></div>';
        }
        if (document.getElementById('notice') == null) {
            document.getElementsByTagName('body')[0].innerHTML += '<div id="notice"></div>';
        }


        var w = getWidth();
        var h = getHeight();
        var t = parseInt(document.documentElement.scrollTop, 10) + document.body.scrollTop;
        top = (h / 2) - (height / 2);
        left = (w / 2) - (width / 2);


        document.getElementById('log').style.height = h + 'px';
        document.getElementById('log').style.visibility = 'visible';
        document.getElementById('notice').style.visibility = 'visible';
        document.getElementById('notice').style.height = height + 'px';
        document.getElementById('notice').style.width = width + 'px';
        document.getElementById('log').style.top = t + 'px';
        document.getElementById('notice').style.top = t + top + 'px';
        document.getElementById('notice').style.left = left + 'px';
        document.getElementById('notice').innerHTML = text;
    }
    function HideWindow() {
        document.getElementById('notice').style.visibility = 'hidden';
        document.getElementById('log').style.visibility = 'hidden';
    }
    function validKey(e) {
        var key = (typeof e.charCode == 'undefined' ? e.keyCode : e.charCode);

        if (e.ctrlKey || e.altKey || key < 32) return true;

        key = String.fromCharCode(key);
        return /[\d]/.test(key);
    }
    BX.ready(function() {
        var jsOffers = '<?=json_encode($arResult['JS_OFFERS']); ?>';
        if (jsOffers != 'null')
            jsOffers = JSON.parse(jsOffers);
        var buy_url = '<?=$arResult['~ADD_URL'];?>';
        function BasketResult() {
            BX.onCustomEvent('OnBasketChange');
            HideWindow();
        }
        var addBtn = document.querySelector('#addToCartButton');
        BX.bind(addBtn,"click",function () {
            ShowWindow("" ,100, 100);
            var basketParams = {
                'ajax_basket': 'Y'
            };
            if (jsOffers != 'null') {
                var index = -1,
                    i = 0,
                    j = 0,
                    boolOneSearch = true;
                var selectedValues = [];
                var size = parseInt($('input[name=skusize]:checked').val());
                var color = parseInt($('.colorSelect').val());
                if (size)
                    selectedValues['PROP_<?=$arResult['SKU_PROPS'][1]['ID']?>'] = size;
                if (color)
                    selectedValues['PROP_<?=$arResult['SKU_PROPS'][0]['ID']?>'] = color;
                for (i = 0; i < jsOffers.length; i++) {
                    boolOneSearch = true;
                    for (j in selectedValues) {
                        if (selectedValues[j] !== jsOffers[i].TREE[j]) {
                            boolOneSearch = false;
                            break;
                        }
                    }
                    if (boolOneSearch) {
                        index = i;
                        break;
                    }
                }
                buy_url = jsOffers[index].BUY_URL;
            }
            basketParams.quantity = document.querySelector('#quantity').value;
            BX.ajax.loadJSON(
                buy_url,
                basketParams,
                BX.delegate(BasketResult, this)
            );

        });
    });
</script>
<?$countItems = count($arResult['SECTION']['ITEMS']);?>
<?$this->SetViewTarget('content2');?>
<?if($countItems > 0):?>
<td class="td_withit"><p class="withit">Данный товар из раздела <a href="<?=$arResult['SECTION']['SECTION_PAGE_URL']?>"><?=$arResult['SECTION']['NAME']?></a>, а также смотрите,
        <?foreach($arResult['SECTION']['ITEMS'] as $key => $arItem):?>
            <a href="<?=$arItem['URL']?>"><?=$arItem['NAME']?></a>
            <?if($countItems != $key + 1):?>
                ,
            <?endif;?>
        <?endforeach;?>
    </p></td>
<?endif;?>
<?$this->EndViewTarget();?>

<script>
    $(function() {
        $(".colorSelect").chosen({
            width:"350px",
            height: "45px",
            disable_search_threshold: 10
        });
    });
</script>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div align="center">

<?
$i = 0;
$count = count($arResult);
foreach($arResult as $arItem):
	$i++;
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
		continue;
?>
	<a style="text-decoration:none" class="style7" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
	<?if($i != $count):?>

	<?endif;?>
<?endforeach?>

</div>
<?endif?>
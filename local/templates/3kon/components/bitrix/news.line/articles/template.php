<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<table width="208" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr>
		<td valign="top"  align="left" class="style7"><div style="padding-left: 5px; padding-top: 5px; padding-bottom: 15px;">
				<ul>
					<?foreach($arResult["ITEMS"] as $arItem):?>
					<li>
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><img width="10" height="9" border="0" align="absmiddle" src="/images/point_4.jpg"><?echo $arItem["NAME"]?></a>
					</li>
					<?endforeach;?>
				</ul>
			</div>
		</td>
	</tr>
	</tbody>
</table>


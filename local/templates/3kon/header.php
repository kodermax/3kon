<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$curPage = $APPLICATION->GetCurPage(true);
CUtil::InitJSCore();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="google-site-verification" content="mijpQdaFyMIkepRRpjjkegT--jDZPt6J5CGyQhipXz0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>
    <?$APPLICATION->SetAdditionalCSS('/style.min.css');?>
    <?$APPLICATION->SetAdditionalCSS('/reset.min.css');?>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div id="log"></div><div id="notice"></div>
<table width="100%" height="100%" BORDER=0 align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div id="header">
                <a href="/" id="logo"></a>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "MENU_THEME" => "site",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                false
            );
                ?>
                   <div id="header-furniture"></div>
            </div>
            <table>	<tr>

                    <td style="padding:10px;">&nbsp;</td>
                    <td class="header_contacts">
                   
						<div class="phone"><span>8(495)&nbsp;796-41-74</span></div>
                        <div class="mailto"><span>&nbsp;<a href="mailto:info@3kon.ru">info@3kon.ru</a></span></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="left" valign="top" >
            <div style="padding-left:2px; padding-top:3px; padding-bottom:17px; padding-right:4px">
                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr align="left" valign="top">
                        <td width="16%" class="teft_area" rowspan="3">
                            <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "basket", Array(
                                    "PATH_TO_BASKET" => SITE_DIR."korzina.html",	// Страница корзины
                                        "PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Персональный раздел
                                        "SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
                                        "SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
                                        "SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
                                        "SHOW_PRODUCTS" => "N",	// Показывать список товаров
                                        "POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
                                    ),
                                    false
                                );?>
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="top" class="left_menu">
                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog", array(
                                            "ROOT_MENU_TYPE" => "left",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "36000000",
                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                            "MENU_THEME" => "site",
                                            "CACHE_SELECTED_ITEMS" => "N",
                                            "MENU_CACHE_GET_VARS" => array(
                                            ),
                                            "MAX_LEVEL" => "3",
                                            "CHILD_MENU_TYPE" => "left",
                                            "USE_EXT" => "Y",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N",
                                        ),
                                            false
                                        );?>
                                    </td>
                                </tr>
                                  <?if ($curPage === SITE_DIR."index.php"):?>
                                        <!-- Отзывы -->
                                    <tr>
                                        <td valign="top" align="left">
                                            <?$APPLICATION->IncludeComponent(
                                                "bitrix:news.list",
                                                "reviews",
                                                Array(
                                                    "IBLOCK_TYPE" => "-",
                                                    "IBLOCK_ID" => "6",
                                                    "NEWS_COUNT" => "2",
                                                    "SORT_BY1" => "ID",
                                                    "SORT_ORDER1" => "ASC",
                                                    "SORT_BY2" => "ID",
                                                    "SORT_ORDER2" => "ASC",
                                                    "FILTER_NAME" => "",
                                                    "FIELD_CODE" => array("",""),
                                                    "PROPERTY_CODE" => array("USER_NAME",""),
                                                    "CHECK_DATES" => "Y",
                                                    "DETAIL_URL" => "",
                                                    "AJAX_MODE" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N",
                                                    "CACHE_TYPE" => "A",
                                                    "CACHE_TIME" => "36000000",
                                                    "CACHE_FILTER" => "N",
                                                    "CACHE_GROUPS" => "Y",
                                                    "PREVIEW_TRUNCATE_LEN" => "",
                                                    "ACTIVE_DATE_FORMAT" => "d.m.Y H:i",
                                                    "SET_STATUS_404" => "N",
                                                    "SET_TITLE" => "Y",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                                    "ADD_SECTIONS_CHAIN" => "Y",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "PARENT_SECTION" => "",
                                                    "PARENT_SECTION_CODE" => "",
                                                    "INCLUDE_SUBSECTIONS" => "Y",
                                                    "DISPLAY_DATE" => "Y",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "Y",
                                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                                    "PAGER_TEMPLATE" => ".default",
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PAGER_SHOW_ALWAYS" => "Y",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "Y"
                                                )
                                            );?>

                                        </td>
                                    </tr>
                                <?endif;?>
                                <tr>
                                    <td valign="top" align="left"><div style="padding-left: 0px; padding-top: 6px;">
                                            <table width="215" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td valign="top" height="30"  align="left" style="background-repeat: no-repeat; font-size: 10px;background-image: url('/images/help.jpg');" class="style8"><div style="padding-left: 60px; padding-top: 14px;">МЕБЕЛЬ ИНФО </div></td>
                                                </tr>

                                                <tr>
                                                    <td valign="top" align="left">
                                                        <div style="padding-left: 7px; padding-top: 0px; padding-right: 0px;">
                                                            <?$APPLICATION->IncludeComponent("bitrix:news.line", "articles", Array(
                                                                "IBLOCK_TYPE" => "articles",	// Тип информационного блока
                                                                "IBLOCKS" => array(	// Код информационного блока
                                                                    0 => "5",
                                                                ),
                                                                "NEWS_COUNT" => "20",	// Количество новостей на странице
                                                                "FIELD_CODE" => array(	// Поля
                                                                    0 => "",
                                                                    1 => "",
                                                                ),
                                                                "SORT_BY1" => "ID",	// Поле для первой сортировки новостей
                                                                "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                                                                "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
                                                                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                                                "DETAIL_URL" => "/stati/#CODE#.html",	// URL, ведущий на страницу с содержимым элемента раздела
                                                                "CACHE_TYPE" => "A",	// Тип кеширования
                                                                "CACHE_TIME" => "300",	// Время кеширования (сек.)
                                                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                                                "ACTIVE_DATE_FORMAT" => "",	// Формат показа даты
                                                            ),
                                                                false
                                                            );?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div></td>
                                </tr>
                            </table>
                        </td>
                        <td width="84%" class="right_area">
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo "<pre>";print_r($arResult);echo "</pre>";

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_stricker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'ZOOM_DIV' => $strMainID.'_zoom_cont',
	'ZOOM_PICT' => $strMainID.'_zoom_pict'
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);

?>
<div class="bx_item_detail" id="<? echo $arItemIDs['ID']; ?>">
<header>
	<h1 class="m-small-marg" itemscope="name">

        <?
        if ('Y' == $arParams['USE_VOTE_RATING'])
        {
            ?>
            <div style="float: right">
            <?$APPLICATION->IncludeComponent(
            "bitrix:iblock.vote",
            "stars",
            array(
                "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                "ELEMENT_ID" => $arResult['ID'],
                "ELEMENT_CODE" => "",
                "MAX_VOTE" => "5",
                "VOTE_NAMES" => array("1", "2", "3", "4", "5"),
                "SET_STATUS_404" => "N",
                "DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
                "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                "CACHE_TIME" => $arParams['CACHE_TIME']
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );?>
    </div>
        <?
        }
        ?>
        <? echo (
        isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && '' != $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
            ? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
            : $arResult["NAME"]
        ); ?>

	</h1>
</header>
<div class="product">
    <div class="product__images">
        <div class="product__big-img-box">
            <a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" onclick="return false;" class="product__big-img-colorbox">
                <img itemprop="image" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="product__big-img" width="400" height="400" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"></a>
        </div>
        <ul class="product__previews">
            <li class="product__previews-item">
                <img itemprop="image" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="product__previews-img" width="75" height="75" alt=<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>">
            </li>
            <?foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto):?>
                <li class="product__previews-item">
                    <img itemprop="image" src="<?=$arOnePhoto['SRC']?>" class="product__previews-img" width="75" height="75" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>">
                </li>
            <?endforeach;?>
        </ul>
    </div>
    <div class="product__settings">
        <h2 class="product__settings-header">Конфигурация</h2>
        <div id="<? echo $arItemIDs['PROP_DIV']; ?>">
            <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][1]['ID']; ?>_cont">
                <div class="alert alert-info">
                    Выберите размер, цвет и доп. опции.
                </div>
                <div class="product__settings-title">Выберите размер</div>
                <ul id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][1]['ID']; ?>_list">
                <?foreach($arResult['SKU_PROPS'][1]['VALUES'] as $arValue):?>
                    <li data-treevalue="<?=$arResult['SKU_PROPS'][1]['ID'].'_'.$arValue['ID']; ?>" data-onevalue="<?=$arValue['ID']; ?>">
                        <div class="product__settings-line">
                            <label class="radio product__settings-item m-product-settings-item">
                                <input class="radio__field" type="radio" name="skusize" data-price="" value="<?=$arValue["XML_ID"]?>">
                                    <span class="radio__label">
                                        <?=$arValue["NAME"]?>
                                    </span>
                            </label>
                        </div>
                    </li>
                <?endforeach;?>
                </ul>
            </fieldset>
            <?

            $firstElement = current($arResult["SKU_PROPS"][0]["VALUES"]);

            ?>
            <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][0]['ID']; ?>_cont">
                <div class="product__settings-title">Выберите цвет</div>
                <div class="selectbox product__settings-colors">
                    <span class="selectbox__label product__settings-colors-label dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=$firstElement['PICT']['SRC']?>" class="color-field" width="30" height="19" alt="">
                        <span class="product__settings-colors-name"><?=$firstElement['NAME'];?></span>
                    </span>
                    <ul class="selectbox__list" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][0]['ID']; ?>_list">
                        <?foreach($arResult["SKU_PROPS"][0]["VALUES"] as $arValue):?>
                            <li class="selectbox__item product__settings-colors-item" data-prop="skucolor"
                                data-treevalue="<?=$arResult['SKU_PROPS'][0]['ID'].'_'.$arValue['ID']; ?>"
                                data-onevalue="<?=$arValue['ID']; ?>">
                                <img src="<?=$arValue["PICT"]["SRC"]?>" class="color-field" width="30" height="19" alt="">
                                <span class="product__settings-colors-name"><?=$arValue["NAME"]?></span>
                            </li>
                        <?endforeach;?>
                    </ul>
                </div>
            </fieldset>
            <?if(count($arResult['SKU_PROPS'][2]["VALUES"]) > 0):?>
             <fieldset class="product__settings-section" id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][2]['ID']; ?>_cont">
                <div class="product__settings-title">Выберите дополнения</div>
                <div class="product__settings-line">
                    <ul id="<? echo $arItemIDs['PROP'].$arResult['SKU_PROPS'][2]['ID']; ?>_list">
                        <?foreach($arResult['SKU_PROPS'][2]["VALUES"] as $arValue):?>
                        <li data-treevalue="<?=$arResult['SKU_PROPS'][2]['ID'].'_'.$arValue['ID']; ?>" data-onevalue="<?=$arValue['ID']; ?>">
                            <label class="radio product__settings-item m-product-settings-item_active">
                                <input class="radio__field" type="radio" name="skuother" value="<?=$arValue['ID']?>" checked="checked" data-price="0">
                                <span class="radio__label"><?=$arValue['NAME']?></span>
                            </label>
                        </li>
                        <?endforeach;?>
                    </ul>
                </div>
            </fieldset>
            <?endif;?>
        </div>
        <div class="product-quantity">
            <div class="spinner">
                <span class="product-quantity__label">Количество</span>
                <span class="btn spinner__decrease spinner__button" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>"><i class="icon-minus"></i></span>
                <input type="text" maxlength="3" class="spinner__field" value="1" id="<? echo $arItemIDs['QUANTITY']; ?>" name="Quantity">
                <span class="btn spinner__increase spinner__button" id="<? echo $arItemIDs['QUANTITY_UP']; ?>"><i class="icon-plus"></i></span>
                <span class="product-quantity__label">шт.</span>
            </div>
        </div>
        <div class="product__settings-total">
            <span class="product__settings-total-price" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></span>
            <div id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: 'none'"></div>
            <div id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: 'none'"></div>
            <span class="product__settings-purchase-button" id="<? echo $arItemIDs['BUY_LINK']; ?>">Добавить в корзину</span>
            <span class="basket__loading"></span>
        </div>
        <div class="product__settings-back">
            <a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" class="product__settings-back-link">вернуться в каталог</a>
        </div>
    </div>
    <br class="clear">
    <article class="product__text" itemprop="description">
        <h2>Дополнительное описание</h2>
        <?if ('' != $arResult['DETAIL_TEXT']):?>
            <? echo $arResult['DETAIL_TEXT'];?>
        <?endif;?>
        <h2>Условия доставки</h2>
        <p>В течение 2 часов после оформления заказа
            (в будние дни и субботу с 10.00 до 20.00) с Вами свяжется наш
            менеджер для подтверждения заказа и уточнит сроки доставки.</p>
        <p>Обычно сроки доставки составляют 1-3 дня, в зависимости от сложности заказа.</p>
    </article>
</div>
</div>
<?
foreach ($arResult['SKU_PROPS'] as &$arProp)
{
    if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
        continue;
    $arSkuProps[] = array(
        'ID' => $arProp['ID'],
        'TYPE' => $arProp['PROPERTY_TYPE'],
        'VALUES_COUNT' => $arProp['VALUES_COUNT']
    );
}
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
    foreach ($arResult['JS_OFFERS'] as &$arOneJS)
    {
        if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
        {
            $arOneJS['PRICE']['PRINT_DISCOUNT_DIFF'] = GetMessage('ECONOMY_INFO', array('#ECONOMY#' => $arOneJS['PRICE']['PRINT_DISCOUNT_DIFF']));
            $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
        }
        $strProps = '';
        if ($arResult['SHOW_OFFERS_PROPS'])
        {
            if (!empty($arOneJS['DISPLAY_PROPERTIES']))
            {
                foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
                {
                    $strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
                        is_array($arOneProp['VALUE'])
                            ? implode(' / ', $arOneProp['VALUE'])
                            : $arOneProp['VALUE']
                        ).'</dd>';
                }
            }
        }
        $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
    }
    if (isset($arOneJS))
        unset($arOneJS);
    $arJSParams = array(
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
        'SHOW_ADD_BASKET_BTN' => true,
        'SHOW_BUY_BTN' => false,
        'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
        'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
        'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
        'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
        'OFFER_GROUP' => $arResult['OFFER_GROUP'],
        'VISUAL' => array(
            'BIG_SLIDER_ID' => $arItemIDs['ID'],
            'ID' => $arItemIDs['ID'],
            'PICT_ID' => $arItemIDs['PICT'],
            'QUANTITY_ID' => $arItemIDs['QUANTITY'],
            'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
            'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
            'QUANTITY_LIMIT' => $arItemIDs['QUANTITY_LIMIT'],
            'PRICE_ID' => $arItemIDs['PRICE'],
            'OLD_PRICE_ID' => $arItemIDs['OLD_PRICE'],
            'DISCOUNT_VALUE_ID' => $arItemIDs['DISCOUNT_PRICE'],
            'DISCOUNT_PERC_ID' => $arItemIDs['DISCOUNT_PICT_ID'],
            'NAME_ID' => $arItemIDs['NAME'],
            'TREE_ID' => $arItemIDs['PROP_DIV'],
            'TREE_ITEM_ID' => $arItemIDs['PROP'],
            'SLIDER_CONT_OF_ID' => $arItemIDs['SLIDER_CONT_OF_ID'],
            'SLIDER_LIST_OF_ID' => $arItemIDs['SLIDER_LIST_OF_ID'],
            'SLIDER_LEFT_OF_ID' => $arItemIDs['SLIDER_LEFT_OF_ID'],
            'SLIDER_RIGHT_OF_ID' => $arItemIDs['SLIDER_RIGHT_OF_ID'],
            'BUY_ID' => $arItemIDs['BUY_LINK'],
            'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_LINK'],
            'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
            'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
            'OFFER_GROUP' => $arItemIDs['OFFER_GROUP'],
            'ZOOM_DIV' => $arItemIDs['ZOOM_DIV'],
            'ZOOM_PICT' => $arItemIDs['ZOOM_PICT']
        ),
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['PREVIEW_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DETAIL_PICTURE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $arSkuProps,
        'AJAX_PATH' => POST_FORM_ACTION_URI,
        'MESS' => array(
            'ECONOMY_INFO' => GetMessage('ECONOMY_INFO')
        )
    );
}
else
{
    $arJSParams = array(
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
        'SHOW_ADD_BASKET_BTN' => true,
        'SHOW_BUY_BTN' => false,
        'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
        'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
        'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
        'VISUAL' => array(
            'BIG_SLIDER_ID' => $arItemIDs['ID'],
            'ID' => $arItemIDs['ID'],
            'PICT_ID' => $arItemIDs['PICT'],
            'QUANTITY_ID' => $arItemIDs['QUANTITY'],
            'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
            'PRICE_ID' => $arItemIDs['PRICE'],
            'OLD_PRICE_ID' => $arItemIDs['OLD_PRICE'],
            'DISCOUNT_VALUE_ID' => $arItemIDs['DISCOUNT_PRICE'],
            'DISCOUNT_PERC_ID' => $arItemIDs['DISCOUNT_PICT_ID'],
            'NAME_ID' => $arItemIDs['NAME'],
            'TREE_ID' => $arItemIDs['PROP_DIV'],
            'TREE_ITEM_ID' => $arItemIDs['PROP'],
            'SLIDER_CONT' => $arItemIDs['SLIDER_CONT_ID'],
            'SLIDER_LIST' => $arItemIDs['SLIDER_LIST'],
            'SLIDER_LEFT' => $arItemIDs['SLIDER_LEFT'],
            'SLIDER_RIGHT' => $arItemIDs['SLIDER_RIGHT'],
            'BUY_ID' => $arItemIDs['BUY_LINK'],
            'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_LINK'],
            'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'PICT' => $arResult['DETAIL_PICTURE'],
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'PRICE' => $arResult['MIN_PRICE'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
            'BUY_URL' => $arResult['~BUY_URL'],
        ),
        'AJAX_PATH' => POST_FORM_ACTION_URI,
        'MESS' => array()
    );
}
?>
<script type="text/javascript">
    var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
    $(".product__previews-item:first").addClass("selected");
    $(".product__big-img").on("load", function(){
        $(".product__big-img").parent().removeClass("loading");
    });
    $(".product__previews-item").on("click", function(e){

        $(".product__previews-item").removeClass("selected");
        var src = $(e.currentTarget).find("img").attr("src");
        $(".product__big-img").parent('.product__big-img-box').addClass("loading");
        $(".product__big-img").attr("src", src);
        $(".product__big-img").parent().attr("href", src);
        $(e.currentTarget).addClass("selected");
    });

    $(".product__big-img-colorbox").colorbox();
</script>


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="catalog">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
           // echo "<pre>";            print_r($arItem);            echo "</pre>";

        ?>
        <li class="catalog__element" itemscope="" itemtype="http://schema.org/Product">
            <div class="catalog__element-header">
                <div class="catalog__element-inner">
                    <figure>
                        <span class="catalog__element-img-box"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" width="205" height="205"></a></span>
                        <figcaption class="catalog__element-name">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog__element-link" title="<?=$arItem['NAME']?>"><?=$arItem["NAME"]?></a>
                        </figcaption>
                    </figure>
                    <div class="catalog___element-desc"><?=$arItem["PREVIEW_TEXT"]?></div>
                </div>
                <?if(!empty($arItem["PROPERTIES"]["NEWPRODUCT"]['VALUE'])):?>
                   <span class="catalog__element-action-text m-catalog-element-action-text_new">новинка</span>
                <?endif;?>
                <?if(!empty($arItem["PROPERTIES"]["SALELEADER"]['VALUE'])):?>
                    <span class="catalog__element-action-text m-catalog-element-action-text_new">популярная модель</span>
                <?endif;?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:iblock.vote",
                    "stars",
                    array(
                        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                        "ELEMENT_ID" => $arItem['ID'],
                        "ELEMENT_CODE" => "",
                        "MAX_VOTE" => "5",
                        "VOTE_NAMES" => array("1", "2", "3", "4", "5"),
                        "SET_STATUS_404" => "N",
                        "DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
                        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                        "CACHE_TIME" => $arParams['CACHE_TIME']
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );?>
            </div>
            <div class="catalog__element-footer">
                <div class="catalog__element-inner" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                    <span class="catalog__element-price m-catalog-element-price_normal" itemprop="price"><?=$arItem["MIN_PRICE"]["PRINT_VALUE"]?></span>
               </div>
            </div></li>
    <?endforeach;?>
</ul>
<?
    if ($arParams["DISPLAY_BOTTOM_PAGER"])
    {
    ?><? echo $arResult["NAV_STRING"]; ?><?
}
?>
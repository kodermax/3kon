<?php

define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER, $APPLICATION;
CUtil::JSPostUnescape();
$APPLICATION->RestartBuffer();
Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
    return;
}
$mode = isset($_POST['MODE']) ? $_POST['MODE'] : '';
if(!isset($mode[0]))
{
    die();
}
$offerID = $_POST["OFFER_ID"] ? $_POST["OFFER_ID"] : '';
$quantity = $_POST["QUANTITY"] ? $_POST["QUANTITY"] : '1';
if(!isset($offerID[0]))
{
    echo CUtil::PhpToJSObject(array('ERROR'=>'ID продукта не найден!'));
    die();
}
if(!isset($quantity[0]))
{
    echo CUtil::PhpToJSObject(array('ERROR'=>'Не указано количество!'));
    die();
}

\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('currency');
\Bitrix\Main\Loader::includeModule('iblock');
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Sale;
use Bitrix\Catalog;
$fUserID = CSaleBasket::GetBasketUserID(True);
$fUserID = IntVal($fUserID);

if ($mode == "ADD2BASKET")
{
    $arRewriteFields = array();
    $db_res = CPrice::GetList(
        array(),
        array(
            "PRODUCT_ID" => $offerID,
            "CATALOG_GROUP_ID" => 2
        )
    );
    if ($ar_res = $db_res->Fetch())
    {
        $arRewriteFields = array('PRICE' => $ar_res['PRICE']);
    }
    $rsItems = CIBlockElement::GetProperty(
        3,
        $offerID,
        array(),
        array('CODE' => 'CML2_LINK')
    );
    if ($arItem = $rsItems->Fetch())
    {
        $productId = $arItem['VALUE'];
    }
    if ($productId > 0)
    {
        $list = Bitrix\Iblock\ElementTable::getList(array(
            'filter' => array('ID' => $productId),
            'select' => array('NAME')
        ));
        if($row = $list->fetch())
        {
            $arRewriteFields['NAME'] = $row['NAME'];
        }
    }
    //Adding sku props
    $db_props = CIBlockElement::GetProperty(3, $offerID, array(), Array("CODE" => "SIZE_REF"));
    if($ar_props = $db_props->Fetch()){
        $sizeId = $ar_props['VALUE'];
    }
    $db_props = CIBlockElement::GetProperty(3, $offerID, array(), Array("CODE" => "COLOR_REF"));
    if($ar_props = $db_props->Fetch()){
        $colorId = $ar_props['VALUE'];
    }
    $db_props = CIBlockElement::GetProperty(3, $offerID, array(), Array("CODE" => "OTHER_REF"));
    if($ar_props = $db_props->Fetch()){
        $otherId = $ar_props['VALUE'];
    }
    //Color
    if ($colorId > 0)
    {
        $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
        $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $query = new \Bitrix\Main\Entity\Query($hlentity);
        $query->setSelect(array('UF_NAME','UF_FILE'))
            ->setFilter(array('UF_XML_ID' => $colorId));
        $result = new CDBResult($query->exec());
        if($row = $result->fetch())
        {
            $arColor = array('NAME' => 'Цвет','CODE' => 'COLOR_REF',"VALUE" => $row['UF_NAME']);
            $filePath = CFile::GetPath($row['UF_FILE']);
            $arImage = array('NAME' => 'Картинка','CODE' => 'COLOR_IMAGE',"VALUE" => $filePath);
        }
    }
    //Size
    if ($sizeId > 0)
    {
        //сначала выбрать информацию о ней из базы данных
        $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(3)->fetch();
        $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $query = new \Bitrix\Main\Entity\Query($hlentity);
        $query->setSelect(array('UF_NAME'))
            ->setFilter(array('UF_XML_ID' => $sizeId));
        $result = new CDBResult($query->exec());
        if($row = $result->fetch())
        {
            $arSize = array('NAME' => 'Размер','CODE' => 'SIZE_REF',"VALUE" => $row['UF_NAME']);
        }
    }
    if ($otherId > 0)
    {
        //сначала выбрать информацию о ней из базы данных
        $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(4)->fetch();
        $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $query = new \Bitrix\Main\Entity\Query($hlentity);
        $query->setSelect(array('UF_NAME'))
            ->setFilter(array('UF_XML_ID' => $otherId));
        $result = new CDBResult($query->exec());
        if($row = $result->fetch())
        {
            $arOther = array('NAME' => 'Доп. опция','CODE' => 'OTHER_REF',"VALUE" => $row['UF_NAME']);
        }
    }
    $arProductParams = array(
       $arColor,
       $arSize,
       $arOther,
       $arImage
    );
    $arRewriteFields["PRODUCT_PROVIDER_CLASS"] = "CCatalogProductProviderCustom";
    if(Add2BasketByProductID($offerID, $quantity, $arRewriteFields, $arProductParams))
    {
        $template = "<ul class='dropdown-basket'>" .
            "<li class='dropdown-basket__item'>В корзине #QUANTITY# товаров</li>" .
            "<li class='dropdown-basket__item dropdown-basket__item-last'>на сумму #ORDERSUM#</li>" .
            "</ul>";
        $templateFooter = "В корзине #QUANTITY# товаров на сумму #ORDERSUM# <a href='/personal/cart/'>Перейти в корзину</a>";
        //Получение кол-во элементов в корзине и сумма заказа
        $num_products = 0;
        $entity = Sale\BasketTable::getEntity();
        $main_query = new Entity\Query($entity);
        $main_query->setSelect(array('QUANTITY','SUMMARY_PRICE'));
        $main_query->setFilter(array('FUSER_ID' => $fUserID,'LID' => SITE_ID,"ORDER_ID" => "NULL"));
        $result = $main_query->exec();
        $allSum = 0.0;
        while ($row = $result->fetch())
        {
            $allSum += $row["SUMMARY_PRICE"];
            $num_products += $row['QUANTITY'];
        }
        $sumFormatted= SaleFormatCurrency($allSum, "RUB");
        $template = str_replace("#QUANTITY#", $num_products, $template);
        $template = str_replace("#ORDERSUM#", $sumFormatted, $template);
        $templateFooter = str_replace("#QUANTITY#", $num_products, $templateFooter);
        $templateFooter = str_replace("#ORDERSUM#", $sumFormatted, $templateFooter);
        $arAddResult = array(
            'STATUS' => 'OK',
            'MESSAGE' => 'Товар успешно добавлен!',
            'CART' => $template,
            'CART_FOOTER' => $templateFooter


        );
    }
    else
    {
        $arAddResult = array(
            'STATUS' => 'ERROR',
            'MESSAGE' => 'Не удалось добавить в корзину'
        );
    }
    $APPLICATION->RestartBuffer();
    echo CUtil::PhpToJSObject($arAddResult);
    die();
}

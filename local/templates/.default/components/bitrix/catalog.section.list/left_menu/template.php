<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<nav>
<ul class="left-menu">
<?foreach($arResult["SECTIONS"] as $arSection):?>
            <li class="left-menu__item">
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="left-menu__link<?if($arSection["IS_SELECTED"]):?> m-left-menu-link_active<?endif;?>"><?=$arSection["NAME"]?></a>
            </li>
<?endforeach;?>
</ul>
</nav>
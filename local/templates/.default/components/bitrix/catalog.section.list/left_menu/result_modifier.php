<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
    if (empty($arResult["SECTIONS"]) && $arResult["SECTION"]["DEPTH_LEVEL"] == 2)
    {

        $rsParentSection = CIBlockSection::GetByID($arResult["SECTION"]["IBLOCK_SECTION_ID"]);
        if ($arParentSection = $rsParentSection->GetNext())
        {
            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
            while ($arSect = $rsSect->GetNext())
            {
                if($arResult["SECTION"]["ID"] == $arSect["ID"])
                    $arSect["IS_SELECTED"] = true;
              $arResult["SECTIONS"][] = $arSect;
            }
        }
    }
if (count($arResult['SECTIONS']) <= 0)
{
    if(in_array($arResult['SECTION']['CODE'], array('top','new','sale')))
    {
        $arResult['SECTIONS'][] = array(
            'NAME' => 'Популярная мебель',
            'SECTION_PAGE_URL' => '/catalog/top/',
            'IS_SELECTED' => $arResult['SECTION']['CODE'] == 'top' ? true : false
        );
        $arResult['SECTIONS'][] = array(
            'NAME' => 'Новинки',
            'SECTION_PAGE_URL' => '/catalog/new/',
            'IS_SELECTED' => $arResult['SECTION']['CODE'] == 'new' ? true : false
        );
        $arResult['SECTIONS'][] = array(
            'NAME' => 'Распродажа',
            'SECTION_PAGE_URL' => '/catalog/sale/',
            'IS_SELECTED' => $arResult['SECTION']['CODE'] == 'sale' ? true : false
        );
    }
}
?>


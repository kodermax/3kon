<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="dropdown-box__inner">
        <ul class="dropdown-basket">
            <?if(Intval($arResult['NUM_PRODUCTS']) > 0):?>
                <li class="dropdown-basket__item"><?echo $arResult["PRODUCTS"]?></li>
            <?else:?>
                <li class="dropdown-basket__item">В корзине 0 товаров</li>
            <?endif;?>
            <li class="dropdown-basket__item dropdown-basket__item-last">на сумму <?=$arResult["SUM"]?></li>
        </ul>
    </div>
    <div class="dropdown-box__bottom">
        <a href="/personal/cart/" class="dropdown-box__link">перейти в корзину</a>
    </div>

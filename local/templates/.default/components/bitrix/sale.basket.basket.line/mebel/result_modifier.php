<?php
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Sale;
Main\Loader::includeModule('sale');
Main\Loader::includeModule('currency');
$fUserID = CSaleBasket::GetBasketUserID(True);
$fUserID = IntVal($fUserID);

$num_products = 0;
$entity = Sale\BasketTable::getEntity();
$main_query = new Entity\Query($entity);
$main_query->setSelect(array('SUMMARY_PRICE'));
$main_query->setFilter(array('FUSER_ID' => $fUserID,'LID' => SITE_ID,"ORDER_ID" => "NULL"));
$result = $main_query->exec();
$allSum = 0.0;
while ($row = $result->fetch())
{
    $allSum += $row["SUMMARY_PRICE"];
}
$arResult["SUM"] = SaleFormatCurrency($allSum, "RUB");
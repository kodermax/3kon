<?php
/**
 * Created by PhpStorm.
 * User: m.karpychev
 * Date: 11.02.14
 * Time: 18:59 
 */

foreach($arResult["GRID"]["ROWS"] as $k => &$arData) {
    foreach($arData['data']['PROPS'] as $arProp)
    {
        if ($arProp['CODE'] == 'COLOR_IMAGE')
        {
            $arData['data']['SKU_VALUES']['COLOR_REF']['SRC'] = $arProp['VALUE'];
        }
        if ($arProp['CODE'] == 'COLOR_REF')
        {
            $arData['data']['SKU_VALUES']['COLOR_REF']['NAME'] = $arProp['VALUE'];
        }
        if ($arProp['CODE'] == 'SIZE_REF')
        {
            $arData['data']['SKU_VALUES']['SIZE_REF'] = array('NAME' => $arProp['VALUE']);
        }
        if ($arProp['CODE'] == 'OTHER_REF')
        {
            $arData['data']['SKU_VALUES']['OTHER_REF'] = array('NAME' => $arProp['VALUE']);
        }
    }
}
function getColorRefData($xmlId)
{
    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
    //затем инициализировать класс сущности
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $query = new \Bitrix\Main\Entity\Query($hlentity);
    $query->setSelect(array('UF_NAME','UF_FILE'))
          ->setFilter(array('UF_XML_ID' => $xmlId));
    $result = $query->exec();
    $result = new CDBResult($result);
    if ($row = $result->fetch())
    {
        $path = '';
        if($row['UF_FILE'] > 0)
            $path = CFile::GetPath($row['UF_FILE']);
        $arData = array(
            'NAME' => $row['UF_NAME'],
            'SRC'  => $path
        );
        return $arData;
    }
    return array();
}

function getSizeRefData($xmlId)
{
    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(3)->fetch();
    //затем инициализировать класс сущности
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $query = new \Bitrix\Main\Entity\Query($hlentity);
    $query->setSelect(array('UF_NAME'))
        ->setFilter(array('UF_XML_ID' => $xmlId));
    $result = $query->exec();
    $result = new CDBResult($result);
    if ($row = $result->fetch())
    {
        $arData = array(
            'NAME' => $row['UF_NAME']);
        return $arData;
    }
    return array();
}
function getOtherRefData($xmlId)
{
    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(4)->fetch();
    //затем инициализировать класс сущности
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $query = new \Bitrix\Main\Entity\Query($hlentity);
    $query->setSelect(array('UF_NAME'))
        ->setFilter(array('UF_XML_ID' => $xmlId));
    $result = $query->exec();
    $result = new CDBResult($result);
    if ($row = $result->fetch())
    {
        $arData = array(
            'NAME' => $row['UF_NAME']);
        return $arData;
    }
    return array();
}
?>

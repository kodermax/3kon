<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>
<div>
    <legend>Укажите дополнительную информацию</legend>
    <textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" rows="7" cols="20" style="width:95%;"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
</div>
    <legend>Информация о заказе</legend>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Наименование товара</th>
            <th>Параметры товара</th>
            <th>Количество</th>
            <th>Цена, руб.</th>
            <th>Сумма, руб.</th>
        </tr>
        </thead>
        <tbody>
        <?foreach($arResult["GRID"]["ROWS"] as $k => $arData):?>
        <tr>
            <td><?=$arData['data']["NAME"]?></td>
            <td>
                <p><?=$arData['data']['SKU_VALUES']['SIZE_REF']['NAME']?></p>
                <p><img src="<?=$arData['data']['SKU_VALUES']['COLOR_REF']['SRC']?>" width="30" height="19" class="color-field"><span class="color__name"><?=$arData['data']['SKU_VALUES']['COLOR_REF']['NAME']?></span></p>
                <p><?=$arData['data']['SKU_VALUES']['OTHER_REF']['NAME']?></p>
            </td>
            <td><?=$arData['data']['QUANTITY']?></td>
            <td><?=$arData['data']['PRICE_FORMATED']?></td>
            <td><?=$arData['data']['SUM']?></td>
        </tr>
    <?endforeach;?>
        <tr class="info">
            <td colspan="5" class="text-right"><strong style="margin-right: 60px">Итого:</strong><strong><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></strong></td>
        </tr>
        </tbody>
    </table>

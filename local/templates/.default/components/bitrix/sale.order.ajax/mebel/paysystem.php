<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<div class="form-horizontal">

		<legend><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></legend>
		<?
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
		{

				if (count($arResult["PAY_SYSTEM"]) == 1)
				{
					?>
                    <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
						<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
							<input type="radio"
								id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
								name="PAY_SYSTEM_ID"
								value="<?= $arPaySystem["ID"] ?>"
								<?if ($arPaySystem["CHECKED"]=="Y") echo " checked=\"checked\"";?>
								/>

							</label>
					<?
				}
				else // more than one
				{
				?>
                 <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
							<input type="radio"
								id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
								name="PAY_SYSTEM_ID"
								value="<?= $arPaySystem["ID"] ?>"
								<?if ($arPaySystem["CHECKED"]=="Y") echo " checked=\"checked\"";?>
								 />
                            <?=$arPaySystem["PSA_NAME"];?>
                </label>
			<?
			}
	}
?>
</div>
<div class="alert alert-info">
    Вы можете оплатить заказ вашей <strong>кредитной картой</strong> курьеру через терминал.
</div>

<?php
/**
 * Created by PhpStorm.
 * User: m.karpychev
 * Date: 11.02.14
 * Time: 12:39 
 */
foreach($arResult['GRID']['ROWS'] as $key => &$arItem)
{
    foreach($arItem['PROPS'] as $arProp)
    {
        if($arProp['CODE'] == 'COLOR_REF')
        {
            $arItem['SKU_VALUES']['COLOR']['NAME'] = $arProp["VALUE"];
        }
        if($arProp['CODE'] == 'COLOR_IMAGE')
        {
            $arItem['SKU_VALUES']['COLOR']['SRC'] = $arProp["VALUE"];
        }
        if($arProp['CODE'] == 'SIZE_REF')
        {
            $arItem['SKU_VALUES']['SIZE']['NAME'] = $arProp["VALUE"];
        }
        if($arProp['CODE'] == 'OTHER_REF')
        {
            $arItem['SKU_VALUES']['OTHER']['NAME'] = $arProp["VALUE"];
        }
    }
}




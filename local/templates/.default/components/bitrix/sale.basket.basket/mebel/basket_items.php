<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;

if ($normalCount > 0):
?>
<div class="cart__data">
		<table class="cart__table">
			<tbody>
				<?
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				?>
                        <tr class="cart__table-row m-cart-table-row_light">
                            <td class="cart__table-cell cart__table-cell-img">
                                <div class="cart__table-img-box">
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>">
                                        <img src="<?=$arItem['DETAIL_PICTURE_SRC']?>" width="83" height="66" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="cart__table-img">
                                    </a>
                                </div>
                            </td>
                            <td colspan="3" class="cart__table-cell cart__table-cell-name">
                                <p>
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
                                </p>
                                <p>
                                    <a class="cart__remove" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"><?=GetMessage("SALE_DELETE")?></a>
                                </p>
                            </td>
                            <td class="cart__table-cell cart__table-cell-info" colspan="2">
                                <p><?=$arItem['SKU_VALUES']['SIZE']['NAME']?></p>
                                <p><img src="<?=$arItem['SKU_VALUES']['COLOR']['SRC']?>" width="30" height="19" class="color-field"><span class="color__name"><?=$arItem['SKU_VALUES']['COLOR']['NAME']?></span></p>
                                <p><?=$arItem['SKU_VALUES']['OTHER']['NAME']?></p>
                            </td>
                            <td class="cart__table-cell cart__table-cell-count">
                                <div class="spinner">
                                    <span class="btn spinner__decrease spinner__button"><i class="icon-minus"></i></span>
                                    <input type="text" maxlength="3" class="spinner__field" value="<?=$arItem["QUANTITY"]?>" name="<? echo "QUANTITY_".$arItem["ID"]?>">
                                    <span class="btn spinner__increase spinner__button"><i class="icon-plus"></i></span>
                                </div>
                            </td>
                            <td class="cart__table-cell cart__table-cell-price">
                                <span class="cart__price"><?=$arItem["PRICE_FORMATED"]?></span>
                            </td>
                        </tr>
					<?
					endif;
				endforeach;
				?>
			</tbody>
		</table>
    <div class="checkout-button-box">
        <div class="product__settings-total cart__checkout-button">
            <span class="product__settings-total-price"><?=$arResult['allSum_FORMATED']?></span>
            <span class="product__settings-purchase-button" onclick="checkOut();">Оформить заказ</span>
        </div>
        <div class="recalculate-button">
            <button type="submit" class="btn" name="BasketRefresh" value="Y"><i class="icon-refresh"></i>Пересчитать</button>
        </div>
    </div>
	</div>
	<input type="hidden" id="QUANTITY_FLOAT" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="COUNT_DISCOUNT_4_ALL_QUANTITY" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="PRICE_VAT_SHOW_VALUE" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="HIDE_COUPON" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="USE_PREPAYMENT" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td colspan="<?=$numCells?>" style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>
<script type="text/javascript">
    $(function(){
        window._gaq = window._gaq || [];

        window._gaq.push(['_trackPageview', '/basket']);

        $(".cart__checkout-button").on("click", function(){

            window._gaq.push(['_trackPageview', '/basket-checkout']);

            MakeOrder();

        });
        $(".spinner").on("click", ".spinner__button", function(e) {
            var elem = $(e.currentTarget),
                field = elem.parent().find(".spinner__field");

            if (elem.hasClass("spinner__decrease")) {

                if (+field.val() > 1) {
                    field.val((+field.val())-1);
                }
            } else {
                field.val((+field.val())+1);
            }
        });
    });
</script>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$curPage = $APPLICATION->GetCurPage(true);
CUtil::InitJSCore();
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="google-site-verification" content="mijpQdaFyMIkepRRpjjkegT--jDZPt6J5CGyQhipXz0" />
    <title><?$APPLICATION->ShowTitle()?></title>
    <!--[if lte IE 8]>
    <script type="text/javascript" src="/js/html5support.js"></script>
    <![endif]-->
    <?$APPLICATION->ShowHead();?>
    <?$APPLICATION->SetAdditionalCSS('/css/style.css');?>
    <?$APPLICATION->AddHeadScript("/js/jquery.js");?>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div id="log"></div><div id="notice"></div>
<div class="wrapper">
    <div class="container">
        <header class="header">
            <div class="line_hold">
                <?if($curPage !== "/index.php"):?>
                    <a class="logo" href="/"><img src="/img/logo.png" width="217" height="59" alt=""/></a>
                <?else:?>
                    <div class="logo">
                        <img src="/img/logo.png" width="217" height="59" alt=""/>
                    </div>
                <?endif;?>
                <? $APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"basket", 
	array(
		"PATH_TO_BASKET" => SITE_DIR."korzina.html",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "N",
		"POSITION_FIXED" => "N",
		"COMPONENT_TEMPLATE" => "basket",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_AUTHOR" => "N",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"POSITION_HORIZONTAL" => "right",
		"POSITION_VERTICAL" => "top"
	),
	false
); ?>
                <!-- end .cart_block -->
                <div class="address_hold">
                    <div class="time_work">
                        Время работы:
                        <div class="time_work_block">
                            Пн-Пт &nbsp;&nbsp;с 9.00 до 21.00
                        </div>
                        <!-- end .time_work_block -->
                        <div class="time_work_bottom">
                            СБ-ВС&nbsp;&nbsp; <strong class="red">с 10.00  до  18.00</strong>
                        </div>
                        <!-- end .time_work_bottom -->
                    </div>
                    <!-- end .time_work -->
                    <div class="top_address">
                        <div class="address_block">
                            <a class="mail" href="mailto:info@3kon.ru">info@3kon.ru</a>
                        </div>
                        <!-- end .address_block -->
                        <div class="address_block">
                            <a class="phone_num" href="tel:84957964174">8 (495) 796-41-74</a>
                            <div class="call_back">
                                <a href="#" class="btn call_btn" title="заказать звонок" rel="nofollow">
                                    Заказать звонок
                                </a>
                            </div>
                            <!-- end .call_back -->
                        </div>
                        <!-- end .address_block -->
                    </div>
                    <!-- end .top_address -->
                </div>
            </div>

            <!-- end .line_hold -->
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                    false
                );
                ?>
            <!-- end .top_menu -->
        </header>
        <!-- end .header -->

        <div class="content">
            <main class="mainContent">


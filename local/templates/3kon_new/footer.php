<!-- end .goods -->
<article class="grey phone_hide">
    <?$APPLICATION->ShowViewContent('article');?>
</article>
<!-- end article -->
<?if ($curPage === SITE_DIR."index.php"):?>
<section class="info_link">
    <h3 class="h2">МЕБЕЛЬ ИНФО</h3>
    <div class="goods_hold">
        <? $APPLICATION->IncludeComponent("bitrix:news.line", "articles", Array(
            "IBLOCK_TYPE" => "articles",    // Тип информационного блока
            "IBLOCKS" => array(    // Код информационного блока
                0 => "5",
            ),
            "NEWS_COUNT" => "4",    // Количество новостей на странице
            "FIELD_CODE" => array(    // Поля
                0 => "PREVIEW_PICTURE"
            ),
            "SORT_BY1" => "ID",    // Поле для первой сортировки новостей
            "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
            "DETAIL_URL" => "/stati/#CODE#.html",    // URL, ведущий на страницу с содержимым элемента раздела
            "CACHE_TYPE" => "A",    // Тип кеширования
            "CACHE_TIME" => "300",    // Время кеширования (сек.)
            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
            "ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
        ),
            false
        ); ?>
        <!-- end .goods_block -->
    </div>
    <!-- end .goods_hold -->
    <div class="aliRight"><a href="/stati.html">Показать все статьи ...</a></div>
</section>
<?endif;?>
<!-- end .info_link -->
</main>
<!-- end .mainContent -->
<aside class="aside">
    <input id="mobile_menu_check" type="checkbox" hidden>
    <div class="mobile_menu">
        <nav class="nav_side">
            <ul>

                <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog", array(
                    "ROOT_MENU_TYPE" => "left",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                ),
                    false
                );?>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "mobile_bottom", Array(
                    "ROOT_MENU_TYPE" => "bottom",    // Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "A",    // Тип кеширования
                    "MENU_CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                ),
                    false  );  ?>
        </nav><!-- end .nav_side -->

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.viewed.products", 
	"vertical", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_PICT_PROP_2" => "MORE_PHOTO",
		"ADDITIONAL_PICT_PROP_3" => "",
		"ADDITIONAL_PICT_PROP_4" => "MORE_PHOTO",
		"ADDITIONAL_PICT_PROP_7" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"BASKET_URL" => "/korzina.html",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CART_PROPERTIES_2" => array(
		),
		"CART_PROPERTIES_3" => "",
		"CART_PROPERTIES_4" => array(
			0 => "",
			1 => "",
		),
		"CART_PROPERTIES_7" => array(
			0 => "",
			1 => "",
		),
		"COMPONENT_TEMPLATE" => "vertical",
		"CONVERT_CURRENCY" => "N",
		"DEPTH" => "",
		"DETAIL_URL" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"LABEL_PROP_2" => "-",
		"LABEL_PROP_4" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"OFFER_TREE_PROPS_3" => "",
		"OFFER_TREE_PROPS_7" => array(
		),
		"PAGE_ELEMENT_COUNT" => "3",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE_2" => array(
		),
		"PROPERTY_CODE_3" => "",
		"PROPERTY_CODE_4" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_7" => array(
			0 => "",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ELEMENT_CODE" => "",
		"SECTION_ELEMENT_ID" => "",
		"SECTION_ID" => "",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_IMAGE" => "Y",
		"SHOW_NAME" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_PRODUCTS_2" => "N",
		"SHOW_PRODUCTS_4" => "Y",
		"TEMPLATE_THEME" => "red",
		"USE_PRODUCT_QUANTITY" => "N"
	),
	false
);?>
    </div>
    <!-- end .mobile_menu -->
    <label class="close_menu" for="mobile_menu_check"></label>
</aside>
<!-- end .aside -->
</div>
<!-- end .content -->
</div>
<!-- end .container -->
<div class="footer-place"></div>
</div>
<!-- end .wrapper -->

<footer class="footer">
    <div class="container">
        <nav class="nav_bottom">
            <?
            $APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                "ROOT_MENU_TYPE" => "bottom",    // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "A",    // Тип кеширования
                "MENU_CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "MENU_THEME" => "site",
                "CACHE_SELECTED_ITEMS" => "N",
                "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
                false
            );
            ?>
        </nav>
        <!-- end .nav_bottom -->
    </div>
    <!-- end .container -->
</footer>
<!-- end .footer -->
<label for="mobile_menu_check" class="menu_label_ico"><span class="nav-toggle"><span></span></span></label>
<?$APPLICATION->IncludeComponent(
    "eva:callback",
    ".default",
    Array(
        "COMPONENT_TEMPLATE" => ".default",
        "EMAIL_TO" => "info@3kon.ru",
        "EVENT_MESSAGE_ID" => "callback",
        "OK_TEXT" => "Спасибо, мы Вам сейчас перезвоним.",
                "REQUIRED_FIELDS" => array(0=>"NAME",1=>"PHONE",),
                "SAVE_FORM_DATA" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_CAPTCHA" => "N",
                "USE_MESSAGE_FIELD" => "N"
        )
);?>
<script type='text/javascript'>
 if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    // код для мобильных устройств
  } else {
<!-- Cleversite chat button -->
                (function() {
                        var s = document.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.charset = 'utf-8';
                        s.src = '//cleversite.ru/cleversite/widget_new.php?supercode=1&referer_main='+encodeURIComponent(document.referrer)+'&clid=14419syytK&siteNew=19966';
                        var ss = document.getElementsByTagName('script')[0];
                        ss.parentNode.insertBefore(s, ss);
                })();
<!-- / End of Cleversite chat button -->
  }
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22658326 = new Ya.Metrika({
                    id:22658326,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22658326" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

</body>
</html>
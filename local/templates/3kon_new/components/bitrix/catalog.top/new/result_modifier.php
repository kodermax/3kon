<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["TD_WIDTH"] = round(100/$arParams["LINE_ELEMENT_COUNT"])."%";
$arResult["nRowsPerItem"] = 1; //Image, Name and Properties
$arResult["bDisplayPrices"] = false;
foreach($arResult["ITEMS"] as &$arItem)
{
	if($arItem['PROPERTIES']['MAIN_PHOTO']['VALUE']){
		$arItem['PROPERTIES']['MAIN_PHOTO']['SRC'] = CFile::GetPath($arItem['PROPERTIES']['MAIN_PHOTO']['VALUE']);
	}
	if (!empty($arItem["PRICES"]) || is_array($arItem["PRICE_MATRIX"]))
		$arResult["bDisplayPrices"] = true;
	elseif (!empty($arItem["OFFERS"]) && is_array($arItem["OFFERS"]))
		$arResult["bDisplayPrices"] = true;
	if($arResult["bDisplayPrices"])
		break;
}


//array_chunk

$arRows = array_chunk($arResult['ITEMS'], $arParams["LINE_ELEMENT_COUNT"]);

$arResult["ROWS"] = $arRows;

?>
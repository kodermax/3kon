<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="goods_hold">
	<?foreach($arResult["ROWS"] as $arItems):?>
		<?foreach($arItems as $arElement):?>
			<?if(is_array($arElement)):?>
				<figure class="goods_block">
					<a href="<?=$arElement['DETAIL_PAGE_URL']?>">
						<span class="top_txt"><?=$arElement['NAME']?></span>
						<span class="img"><img src="<?=$arElement['PROPERTIES']["MAIN_PHOTO"]["SRC"]?>" width="200" height="200" alt=""/></span>
						<span class="txt"><?=$arElement['NAME']?></span>
					</a>
				</figure>
				<?endif;?>
			<?endforeach;?>
	<?endforeach;?>
</div>

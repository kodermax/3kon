<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?><br/>
<? endif; ?>
<ul class="reviews_list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <li>
            <div class="reviews_name"> <?= $arItem['PROPERTIES']['USER_NAME']['VALUE'] ?> </div>
            <!-- end .reviews_name -->
            <div class="nofloat">
                <div class="reviews_head"><strong><?= $arItem['NAME'] ?></strong>
                    <time><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></time>
                </div>
                <!-- end .reviews_head -->
                <?= $arItem['PREVIEW_TEXT'] ?>
            </div>
            <!-- end .nofloat -->
        </li>
    <? endforeach; ?>
</ul>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <br/><?= $arResult["NAV_STRING"] ?>
<? endif; ?>


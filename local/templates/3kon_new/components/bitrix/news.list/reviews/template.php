<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<label class="info_link_section" for="rew1">ОТЗЫВЫ</label>
<input id="rew1" type="checkbox" hidden>
<div class="mobile_hide">
    <ul>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <li><?= $arItem['PROPERTIES']['USER_NAME']['VALUE'] ?> (<strong><?= $arItem['NAME'] ?></strong>):
                <time>(<?= $arItem['DISPLAY_ACTIVE_FROM'] ?>)</time>
                <p><?= $arItem['PREVIEW_TEXT'] ?></p>
            </li>
        <? endforeach; ?>
    </ul>
    <a href="/otzyvy.html">Все отзывы</a>
</div>

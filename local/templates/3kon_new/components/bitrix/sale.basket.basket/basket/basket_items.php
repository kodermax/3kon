<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0):
    ?>
    <table id="basket_items" class="cart_table">
        <thead>
        <tr>
            <?
            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                $arHeaders[] = $arHeader["id"];

                // remember which values should be shown not in the separate columns, but inside other columns
                if (in_array($arHeader["id"], array("TYPE"))) {
                    $bPriceType = true;
                    continue;
                } elseif ($arHeader["id"] == "PROPS") {
                    $bPropsColumn = true;
                    continue;
                } elseif ($arHeader["id"] == "DELAY") {
                    $bDelayColumn = true;
                    continue;
                } elseif ($arHeader["id"] == "DELETE") {
                    $bDeleteColumn = true;
                    continue;
                } elseif ($arHeader["id"] == "WEIGHT") {
                    $bWeightColumn = true;
                }

                if ($arHeader["id"] == "NAME"):
                    ?>
                    <th id="col_<?= getColumnId($arHeader) ?>">
                    <?
                elseif ($arHeader["id"] == "PRICE"):
                    ?>
                    <th id="col_<?= getColumnId($arHeader) ?>">
                    <?
                else:
                    ?>
                    <th id="col_<?= getColumnId($arHeader) ?>">
                    <?
                endif;
                ?>
                <b><?= getColumnName($arHeader) ?></b>
                </th>
                <?
            endforeach;

            if ($bDeleteColumn || $bDelayColumn):
                ?>
                <th class="nopheader custom"></th>
                <?
            endif;
            ?>
        </tr>
        </thead>
        <tbody>
        <?
        foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

            if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                ?>
                <tr id="<?= $arItem["ID"] ?>">
                    <?
                    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                        if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
                            continue;

                        if ($arHeader["id"] == "NAME"):
                            ?>
                            <td>
                                    <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?><a
                                        href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?endif;
                                        ?>
                                        <?= $arItem["NAME"] ?>
                                        <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?></a><?endif;
                                ?>
                                <div class="bx_ordercart_itemart">
                                    <?
                                    if ($bPropsColumn):
                                        foreach ($arItem["PROPS"] as $val):

                                            if (is_array($arItem["SKU_DATA"])) {
                                                $bSkip = false;
                                                foreach ($arItem["SKU_DATA"] as $propId => $arProp) {
                                                    if ($arProp["CODE"] == $val["CODE"]) {
                                                        $bSkip = true;
                                                        break;
                                                    }
                                                }
                                                if ($bSkip)
                                                    continue;
                                            }

                                            echo $val["NAME"] . ":&nbsp;<span>" . $val["VALUE"] . "<span><br/>";
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                            </td>
                            <?
                        elseif ($arHeader["id"] == "QUANTITY"):
                            ?>
                            <td class="centr">
                                                <?
                                                $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                                $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $arItem["AVAILABLE_QUANTITY"] . "\"" : "";
                                                $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                                $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                                                ?>
                                                <input
                                                    type="text"
                                                    size="3"
                                                    id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                                    name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                                    size="2"
                                                    maxlength="18"
                                                    min="0"
                                                    <?= $max ?>
                                                    step="<?= $ratio ?>"
                                                    style="max-width: 50px"
                                                    value="<?= $arItem["QUANTITY"] ?>"
                                                    onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                                                >
                                <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>"
                                       name="QUANTITY_<?= $arItem['ID'] ?>" value="<?= $arItem["QUANTITY"] ?>"/>
                            </td>
                            <?
                        elseif ($arHeader["id"] == "PRICE"):
                            ?>
                            <td>
                                <div class="current_price" id="current_price_<?= $arItem["ID"] ?>">
                                    <?= $arItem["PRICE_FORMATED"] ?>
                                </div>
                                <div class="old_price" id="old_price_<?= $arItem["ID"] ?>">
                                    <? if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
                                        <?= $arItem["FULL_PRICE_FORMATED"] ?>
                                    <?endif; ?>
                                </div>

                                <? if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
                                    <div class="type_price"><?= GetMessage("SALE_TYPE") ?></div>
                                    <div class="type_price_value"><?= $arItem["NOTES"] ?></div>
                                <?endif; ?>
                            </td>
                            <?
                        else:
                            ?>
                            <td>
                                <?
                                if ($arHeader["id"] == "SUM"):
                                ?>
                                <div id="sum_<?= $arItem["ID"] ?>">
                                    <?
                                    endif;

                                    echo $arItem[$arHeader["id"]];

                                    if ($arHeader["id"] == "SUM"):
                                    ?>
                                </div>
                            <?
                            endif;
                            ?>
                            </td>
                            <?
                        endif;
                    endforeach;

                    if ($bDelayColumn || $bDeleteColumn):
                        ?>
                        <td class="centr">
                            <?
                            if ($bDeleteColumn):
                                ?>
                                <a class="btn btn_grey"
                                   href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>">X</a><br/>
                                <?
                            endif;
                            ?>
                        </td>
                        <?
                    endif;
                    ?>
                </tr>
                <?
            endif;
        endforeach;
        ?>
        <tr>
            <td colspan="3"><b>Итого</b></td>
            <td colspan="2"><b id="allSum_FORMATED"
                               class="red"><?= str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]) ?></b></td>
        </tr>
        </tbody>
    </table>
    <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
    <input type="hidden" id="offers_props" value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
    <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
    <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
    <input type="hidden" id="count_discount_4_all_quantity"
           value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="price_vat_show_value"
           value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="coupon_approved" value="N"/>
    <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>

    <div class="aliRight"><a href="javascript:void(0)" onclick="checkOut();" class="btn btn_blue">Оформить заказ</a>
    </div>
    <?
else:
    ?>
    <div id="basket_items_list">
    <table>
        <tbody>
        <tr>
            <td colspan="<?= $numCells ?>" style="text-align:center">
                <div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
            </td>
        </tr>
        </tbody>
    </table>
    <?
endif;
?>
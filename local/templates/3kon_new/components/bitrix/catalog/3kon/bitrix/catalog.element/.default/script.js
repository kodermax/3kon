$(document).ready(function() {
    $(".fancybox").fancybox({
        padding : 0,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $(".colorSelect").chosen({
        width:"350px",
        height: "45px",
        disable_search_threshold: 10
    });
});

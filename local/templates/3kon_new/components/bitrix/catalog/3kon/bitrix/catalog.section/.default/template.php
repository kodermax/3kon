<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <div class="category_item_head">
        <span class="category_header1">Артикул</span>
        <span class="category_header2">Наименование <a class="arrow" href="?sort=name&order=asc">▲</a> <a class="arrow"
                                                                                       href="?sort=name&order=desc">▼</a></span>
        <span class="category_header4">Цена <a class="arrow" href="?sort=price&order=asc">▲</a> <a class="arrow" href="?sort=price&order=desc">▼</a></span>
        <span class="category_header3">Ед. изм.</span>
    </div>
    <ul class="category_item_hold">
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <li><span class="category_item1"><?= $arItem['PROPERTIES']['ARTICLE']['VALUE'] ?></span>
                <a class="img" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""></a>
                <span class="category_price"><?= $arItem["MIN_PRICE"]["PRINT_VALUE"] ?></span>
                <div class="category_block"><a class="category_link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                    <p class="category_txt"> <?= $arItem['PREVIEW_TEXT'] ?> </p>
                    <!-- end .category_txt -->
                </div>
                <!-- end .category_block -->
                <span class="category_item2">шт.</span></li>
        <? endforeach; ?>
    </ul>
<?
if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
    ?><? echo $arResult["NAV_STRING"]; ?><?
}
?>
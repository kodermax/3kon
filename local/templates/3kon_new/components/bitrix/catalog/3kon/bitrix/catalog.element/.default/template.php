<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
    'STICKER_ID' => $strMainID . '_stricker',
    'BIG_SLIDER_ID' => $strMainID . '_big_slider',
    'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
    'SLIDER_LIST' => $strMainID . '_slider_list',
    'SLIDER_LEFT' => $strMainID . '_slider_left',
    'SLIDER_RIGHT' => $strMainID . '_slider_right',
    'OLD_PRICE' => $strMainID . '_old_price',
    'PRICE' => $strMainID . '_price',
    'DISCOUNT_PRICE' => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_DOWN' => $strMainID . '_quant_down',
    'QUANTITY_UP' => $strMainID . '_quant_up',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
    'BUY_LINK' => $strMainID . '_buy_link',
    'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
    'COMPARE_LINK' => $strMainID . '_compare_link',
    'PROP' => $strMainID . '_prop_',
    'PROP_DIV' => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'OFFER_GROUP' => $strMainID . '_set_group_',
    'ZOOM_DIV' => $strMainID . '_zoom_cont',
    'ZOOM_PICT' => $strMainID . '_zoom_pict',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);
?>
<h1 class="red"><?= $arResult['NAME'] ?></h1>
<div class="product_hold">
    <div class="clearfix"><a class="img img_big fancybox" href="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" rel="group1"><img
                src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt=""></a>
        <div class="product_block">
            <? if (!empty($arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE'])): ?>
                <p>Артикул: <?= $arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE'] ?></p>
            <? endif; ?>
            <? if ($arResult['IS_SKU']): ?>
                <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
                        ?>
                        <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                               value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                        <?
                        if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                            unset($arResult['PRODUCT_PROPERTIES'][$propID]);
                    } ?>
                </div>
                <h2 class="product__settings-header">Конфигурация</h2>
                <div class="sku_div" id="<? echo $arItemIDs['PROP_DIV']; ?>">
                    <? if (array_key_exists('SIZE_REF', $arResult['SKU_PROPS'])): ?>
                        <div class="product__settings-title">Выберите размер</div>
                        <ul class="sku_list" id="<?= 'PROP_' . $arResult['SKU_PROPS']['SIZE_REF']['ID']; ?>">
                            <? foreach ($arResult['SKU_PROPS']['SIZE_REF']['VALUES'] as $arValue): ?>
                                <li data-treevalue="<?= $arResult['SKU_PROPS'][1]['ID'] . '_' . $arValue['ID']; ?>"
                                    data-onevalue="<?= $arValue['ID']; ?>">
                                    <label class="radio product__settings-item m-product-settings-item">
                                        <input <? if (++$i == 1): ?>checked="checked"<? endif; ?> class="radio__field"
                                               type="radio" name="sku_size" data-price=""
                                               value="<?= $arValue["ID"] ?>">
                                    <span class="radio__label">
                                        <?= $arValue["NAME"] ?>
                                    </span>
                                    </label>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>

                    <? if (array_key_exists('COLOR_REF', $arResult['SKU_PROPS'])): ?>
                        <?
                        $firstElement = current($arResult["SKU_PROPS"]['COLOR_REF']["VALUES"]);
                        ?>
                        <div class="product__settings-title">Выберите цвет</div>
                        <select class="colorSelect" name="sku_color">
                            <? foreach ($arResult["SKU_PROPS"]['COLOR_REF']["VALUES"] as $arValue): ?>
                                <option value="<?= $arValue['ID'] ?>"
                                        data-img-src="<?= $arValue["PICT"]["SRC"] ?>"><?= $arValue["NAME"] ?></option>
                            <? endforeach ?>
                        </select>
                    <? endif; ?>
                    <? if (array_key_exists('OTHER_REF', $arResult['SKU_PROPS'])): ?>
                        <? if (count($arResult['SKU_PROPS']['OTHER_REF']["VALUES"]) > 0): ?>
                            <div class="product__settings-title">Выберите дополнения</div>
                            <div class="product__settings-line">
                                <ul class="sku_list" id="<?= 'PROP_' . $arResult['SKU_PROPS'][2]['ID']; ?>">
                                    <? foreach ($arResult['SKU_PROPS']['OTHER_REF']["VALUES"] as $arValue): ?>
                                        <li data-treevalue="<?= $arResult['SKU_PROPS']['OTHER_REF']['ID'] . '_' . $arValue['ID']; ?>"
                                            data-onevalue="<?= $arValue['ID']; ?>">
                                            <label class="radio product__settings-item m-product-settings-item_active">
                                                <input class="radio__field" type="radio" name="sku_other"
                                                       value="<?= $arValue['ID'] ?>" checked="checked" data-price="0">
                                                <span class="radio__label"><?= $arValue['NAME'] ?></span>
                                            </label>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                    <? endif; ?>

                </div>
            <? endif; ?>
            <p>Цена: <span class="green" id="price"><?= $arResult['MIN_PRICE']['PRINT_VALUE'] ?></span></p>
            <div class="btn_hold">
                <input id="quantity" onkeypress="return validKey(event)" class="price_input" type="text" value="1">
                <a id="addToCartButton" class="btn" href="javascript:void(0);">В корзину</a>
                <br/>
                <a id="fastOrder" class="btn" style="margin-top:10px;color:black;background-color:#ebf4f8" href="javascript:void(0)">Быстрый заказ</a>
            </div>

            <!-- end .btn_hold -->
            <p class="info_text"><span class="recall">Появились вопросы? <span
                        style="font-size: 16px;font-weight: bold">Позвоните нам!</span></span> <br>
                <a class="phone_num" href="tel:4957964174">(495) 796-41-74</a></p>
        </div>
        <!-- end .product_block -->
    </div>
    <!-- end .clearfix -->
    <div class="img_hold">
        <? foreach ($arResult['MORE_PHOTO'] as $key => $item): ?>
            <a class="img img_small fancybox" href="<?= $item['SRC'] ?>" rel="group1">
                <img src="<?= $item['SRC'] ?>" width="700" height="700" alt="">
            </a>
        <? endforeach; ?>
    </div>
    <!-- end .img_hold -->
</div>
<article class="product_text">
    <?= $arResult['DETAIL_TEXT'] ?>
    <!-- end .prod_list -->
</article>
<div class="fast_form" id="fast_order_popup" style="display:none;">
    <h3>Быстрый заказ</h3>
    <p class="fio">
        <label>ФИО: <span class="red">*</span></label><input name="fast_fio" type="text">
        <span class="error required">Поле ФИО не заполнено</span>
    </p>
    <p class="phone">
        <label>Телефон: <span class="red">*</span></label><input name="fast_phone" type="text">
        <span class="error required">Поле Телефон не заполнено</span>
    </p>
    <p class="email"><label>E-Mail:</label><input name=fast_mail type="text"></p>
    <p><input onclick="return FastSend();" type="submit" value="Отправить" /></p>
</div>
<div id="success" class="fast_form" style="display:none;">
    <h3>Сообщение</h3>
    <p><span style="color:green;font-weight:bold">Заказ отправлен!</span>
        <br/>В ближайшее время с вами свяжется наш менеджер!</p>
</div>
<script type="text/javascript">
    function FastSend(){
        var fio = $("input[name='fast_fio']").val();
        var phone = $("input[name='fast_phone']").val();
        var mail = $("input[name='fast_mail']").val();
        if(!fio && fio.length <= 0) {
            $('.fast_form .fio .error.required').show();
        }
        else {
            $('.fast_form .fio .error.required').hide();
        }
        if(!phone && phone.length <= 0) {
            $('.fast_form .phone .error.required').show();
        }
        else {
            $('.fast_form .phone .error.required').hide();
        }
        var quantity = document.querySelector('#quantity').value;
        $.ajax(
            {
                type: 'POST',
                url: "/local/ajax/fast_order.php",
                data: {
                    fio: fio,
                    phone: phone,
                    mail: mail,
                    offerId: mySku.Id,
                    quantity: quantity,
                    mode: 'FAST_ORDER'
                },
                dataType: 'json',
                success: function(response){
                    if (response.result){
                        $("#fast_order_popup").modal('close');
                        $("#success").modal({
                            modalClass: 'modal_dialog',
                        });
                    }
                }
            }
        );

    }
    $('#fastOrder').click(function(){
        $("#fast_order_popup").modal({
            modalClass: 'modal_dialog',
        });
    });

    function ShowWindow(text, width, height) {
        var top;
        var left;

        if (width == "") width = "150";
        if (height == "") height = "100";
        if (text == "") text = "";

        // --------------- Browsers
        isDOM = document.getElementById; //DOM1 browser (MSIE 5+, Netscape 6, Opera 5+)
        isOpera = isOpera5 = window.opera && isDOM;//Opera 5+
        isOpera6 = isOpera && window.print; //Opera 6+
        isOpera7 = isOpera && document.readyState; //Opera 7+
        isMSIE = document.all && document.all.item && !isOpera; //Microsoft Internet Explorer 4+
        isMSIE5 = isDOM && isMSIE; //MSIE 5+
        isNetscape4 = document.layers; //Netscape 4.*
        isMozilla = isDOM && navigator.appName == "Netscape"; //Mozilla & Netscape 6.*
        // -- Get height

        function getHeight() { // Получаем высоту рабочей области браузера
            if (isMSIE || isMozilla || isMSIE) send = document.documentElement.clientHeight;
            if (isOpera6 || isOpera7 || isOpera || isNetscape4) send = window.innerHeight;
            return send;
        }

        // -- Get width

        function getWidth() { // Получаем высоту рабочей области браузера
            if (isMSIE || isMozilla || isMSIE) send = document.documentElement.clientWidth;
            if (isOpera6 || isOpera7 || isOpera || isNetscape4) send = window.innerWidth;
            return send;
        }

        //-----------------------------
        if (document.getElementById('log') == null) {
            document.getElementsByTagName('body')[0].innerHTML += '<div id="log"></div>';
        }
        if (document.getElementById('notice') == null) {
            document.getElementsByTagName('body')[0].innerHTML += '<div id="notice"></div>';
        }


        var w = getWidth();
        var h = getHeight();
        var t = parseInt(document.documentElement.scrollTop, 10) + document.body.scrollTop;
        top = (h / 2) - (height / 2);
        left = (w / 2) - (width / 2);


        document.getElementById('log').style.height = h + 'px';
        document.getElementById('log').style.visibility = 'visible';
        document.getElementById('notice').style.visibility = 'visible';
        document.getElementById('notice').style.height = height + 'px';
        document.getElementById('notice').style.width = width + 'px';
        document.getElementById('log').style.top = t + 'px';
        document.getElementById('notice').style.top = t + top + 'px';
        document.getElementById('notice').style.left = left + 'px';
        document.getElementById('notice').innerHTML = text;
    }
    function HideWindow() {
        document.getElementById('notice').style.visibility = 'hidden';
        document.getElementById('log').style.visibility = 'hidden';
    }
    function validKey(e) {
        var key = (typeof e.charCode == 'undefined' ? e.keyCode : e.charCode);

        if (e.ctrlKey || e.altKey || key < 32) return true;

        key = String.fromCharCode(key);
        return /[\d]/.test(key);
    }
    var mySku = {};
    mySku.Id = '<?=$arResult['ID']?>';
    mySku.size = null;
    mySku.color = null;
    mySku.other = null;
    mySku.buyUrl = '<?=$arResult['~ADD_URL'];?>';
    mySku.price = 0;
    mySku.priceElement = $("#price");
    mySku.getSelected = function () {
        this.size = parseInt($("input[name=sku_size]:checked").val());
        this.color = parseInt($('.colorSelect').val());
        this.other = parseInt($('input[name=sku_other]:checked').val());
    };
    mySku.updatePrice = function () {
        this.getSelected();

        var selectedValues = [];
        if (this.size)
            selectedValues['PROP_59'] = this.size;
        if (this.color)
            selectedValues['PROP_58'] = this.color;
        if (this.other)
            selectedValues['PROP_60'] = this.other;
        var index = -1;
        for (var i = 0; i < this.jsOffers.length; i++) {
            var boolOneSearch = true;
            for (j in selectedValues) {
                if (selectedValues[j] !== this.jsOffers[i].TREE[j]) {
                    boolOneSearch = false;
                    break;
                }
            }
            if (boolOneSearch) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            this.Id = this.jsOffers[index].ID;
            this.buyUrl = this.jsOffers[index].BUY_URL;
            this.price = this.jsOffers[index].PRICE.PRINT_VALUE;
            mySku.priceElement.text(this.price);
        }
    };
    mySku.updateSku = function () {
        var selectedSize = $("input[name=sku_size]:checked").val();
        var Colors = [];
        var Others = [];
        $.each(this.jsOffers, function (key, value) {
            if (value['TREE']['PROP_59'] == selectedSize) {
                if (value['TREE']['PROP_58'])
                    Colors.push(value['TREE']['PROP_58']);
                if (value['TREE']['PROP_60'])
                    Others.push(value['TREE']['PROP_60']);
            }
        });
        //Sku Colors
        $(".colorSelect option").each(function (i) {
            var el = $(this);
            if (Colors.indexOf(parseInt(el.val())) < 0) {
                el.hide();
            }
        });
        //Sku Others
        if (Others.length > 0) {
            $("#PROP_60").find("li").each(function (index, element) {
                var el = $(element);
                if (Others.indexOf(parseInt(el.attr('data-onevalue'))) < 0) {
                    el.hide();
                }
            });
        }
        this.updatePrice();
    };

    BX.ready(function () {
        mySku.jsOffers = '<?=json_encode($arResult['JS_OFFERS']); ?>';
        if (mySku.jsOffers != 'null') {
            mySku.jsOffers = JSON.parse(mySku.jsOffers);
            if (mySku.jsOffers.length > 0) {
                mySku.updateSku();
                $('input[type=radio][name=sku_size]').change(function () {
                    mySku.updateSku();
                });
                $('input[type=radio][name=sku_other]').change(function () {
                    mySku.updatePrice();
                });
                $('.colorSelect').change(function () {
                    mySku.updatePrice();
                });
            }

        }
        function BasketResult() {
            BX.onCustomEvent('OnBasketChange');
            HideWindow();
        }


        $('#addToCartButton').click(function () {
            ShowWindow("", 100, 100);
            var basketParams = {
                'ajax_basket': 'Y'
            };
            basketParams.quantity = document.querySelector('#quantity').value;
            BX.ajax.loadJSON(
                mySku.buyUrl,
                basketParams,
                BX.delegate(BasketResult, this)
            );
        });
    });
</script>


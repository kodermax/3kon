<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<nav class="submenu">
    <h1 class="submenu__header">Каталог мебели</h1>
    <ul class="submenu__list">
        <li class="submenu__item"><span class="submenu__link m-submenu-link_active">Вся мебель</span></li>
        <li class="submenu__item"><a href="/catalog/sale/" class="submenu__link">Распродажа</a></li>
        <li class="submenu__item"><a href="/catalog/new/" class="submenu__link">Новинки</a></li>
        <li class="submenu__item"><a href="/catalog/top/" class="submenu__link">Популярные модели</a></li>
    </ul>
</nav>
<section class="catalog-sections">
    <?foreach($arResult["SECTIONS"] as $arSection):?>
    <div class="catalog-sections__item">
        <div class="catalog-sections__inner">
            <figure class="catalog-sections__figure">
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><img src="<?=$arSection["PICTURE"]["SRC"]?>" class="catalog-sections__img" width="100%" height="auto" alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>"></a>
                <figcaption class="catalog-sections__name">
                    <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog-sections__link" title="<?=$arSection["NAME"]?>"><?=$arSection["NAME"]?></a>
                    <span class="catalog-sections_desc"></span>
                </figcaption>
            </figure>
        </div>
    </div>
    <?endforeach;?>
</section>

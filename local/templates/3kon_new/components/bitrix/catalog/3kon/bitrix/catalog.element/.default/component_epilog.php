<?
global $APPLICATION;
//$APPLICATION->AddHeadScript("/js/jquery-2.1.4.min.js");
$APPLICATION->AddHeadScript("/js/chosen.jquery.min.js");
$APPLICATION->AddHeadScript("/js/highslide-full.min.js");
$APPLICATION->AddHeadScript("/js/ImageSelect/ImageSelect.jquery.js");
$APPLICATION->SetAdditionalCSS("/js/highslide.css");
$APPLICATION->SetAdditionalCSS("/css/chosen.min.css");
$APPLICATION->SetAdditionalCSS("/js/ImageSelect/ImageSelect.css");
$APPLICATION->AddHeadScript("/js/jquery.mousewheel.min.js");
$APPLICATION->AddHeadScript("/js/fancybox/jquery.fancybox.js");
$APPLICATION->AddHeadScript("/js/jquery.modal.js");
$APPLICATION->SetAdditionalCSS("/js/fancybox/jquery.fancybox.css");
$APPLICATION->SetAdditionalCSS("/css/jquery.modal.css");
?>
<script type="text/javascript">
// override Highslide settings here
// instead of editing the highslide.js file
    hs.graphicsDir = '/js/graphics/';
</script>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0): ?>
	<?
	$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
	$arNotify = unserialize($notifyOption);
	?>
	<?if ($arParams["FLAG_PROPERTY_CODE"] == "NEWPRODUCT"):?>
		<h3 class="newsale"><span></span><?=GetMessage("CR_TITLE_".$arParams["FLAG_PROPERTY_CODE"])?></h3>
	<?elseif (strlen($arParams["FLAG_PROPERTY_CODE"]) > 0):?>
    	<h3 class="hitsale"><span></span><?=GetMessage("CR_TITLE_".$arParams["FLAG_PROPERTY_CODE"])?></h3>
	<?endif?>
<div class="listitem-carousel">
	<div class="goods_hold">

<?foreach($arResult["ITEMS"] as $key => $arItem):
	if(is_array($arItem))
	{
		$bPicture = is_array($arItem["PREVIEW_IMG"]);
        ?><figure class="goods_block">
			<?if ($bPicture):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<span class="top_txt"><?=$arItem['NAME']?></span>
					<span class="img"><img class="item_img" itemprop="image" src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" width="200" height="200"" alt="<?=$arItem["NAME"]?>" /></span>
				</a>
			<?else:?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><div class="no-photo-div-big" style="height:130px; width:130px;"></div></a>
			<?endif?>
			<div class="buy">
				<div class="price" itemprop = "offers" itemscope itemtype = "http://schema.org/Offer"><?
				if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]))   //if product has offers
				{
					if (count($arItem["OFFERS"]) > 1)
					{
                    ?>
                       <span itemprop = "price" class="item_price" style="color:#000">
                    <?
						echo GetMessage("CR_PRICE_OT")."&nbsp;";
						echo $arItem["PRINT_MIN_OFFER_PRICE"];
                    ?>
                        </span>
                    <?
					}
					else
					{
						foreach($arItem["OFFERS"] as $arOffer):?>
							<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
								<?if($arPrice["CAN_ACCESS"]):?>
										<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<span itemprop = "discount-price" class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span><br>
											<span class="old-price"><?=$arPrice["PRINT_VALUE"]?></span><br>
											<?else:?>
											<span itemprop = "price" class="item_price price"><?=$arPrice["PRINT_VALUE"]?></span>
										<?endif?>
								<?endif;?>
							<?endforeach;?>
						<?endforeach;
					}
				}
				else // if product doesn't have offers
				{
                    foreach($arItem["PRICES"] as $code=>$arPrice):
                        if($arPrice["CAN_ACCESS"]):
                            ?>
                                <?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                    <span itemprop = "price" class="item_price discount-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span><br>
                                    <span itemprop = "price" class="old-price"><?=$arPrice["PRINT_VALUE"]?></span>
                                <?else:?>
                                    <span itemprop = "price" class="item_price price"><?=$arPrice["PRINT_VALUE"]?></span>
                                <?endif;?>
                            <?
                        endif;
                    endforeach;
				}
				?>
				</div>
				<br/>
				<?if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]))
				{
				?>
                    <noindex><a href="javascript:void(0)" class="bt3 addtoCart" id="catalog_add2cart_offer_link_<?=$arItem['ID']?>" onclick="return showOfferPopup(this, 'list', '<?=GetMessage("CATALOG_IN_CART")?>', <?=CUtil::PhpToJsObject($arItem["SKU_ELEMENTS"])?>, <?=CUtil::PhpToJsObject($arItem["SKU_PROPERTIES"])?>, <?=CUtil::PhpToJsObject($arResult["POPUP_MESS"])?>, 'cart');"><?echo GetMessage("CATALOG_ADD")?></a></noindex>
				<?
				}
				else
				{
				?>
					<?if ($arItem["CAN_BUY"]):?>
						<noindex><a href="<?=$arItem["ADD_URL"]?>" class="bx_bt_button bx_medium" rel="nofollow" onclick="return addToCart(this, 'list', '<?=GetMessage("CATALOG_IN_CART")?>', 'noCart');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?=GetMessage("CATALOG_ADD")?></a></noindex>
					<?elseif ($arNotify[SITE_ID]['use'] == 'Y'):?>
						<?if ($USER->IsAuthorized()):?>
							<noindex><a href="<?echo $arItem["SUBSCRIBE_URL"]?>" rel="nofollow" class="subscribe_link" onclick="return addToSubscribe(this, '<?=GetMessage("CATALOG_IN_SUBSCRIBE")?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
						<?else:?>
                        	<noindex><a href="javascript:void(0)" rel="nofollow" class="subscribe_link" onclick="showAuthForSubscribe(this, <?=$arItem['ID']?>, '<?echo $arItem["SUBSCRIBE_URL"]?>')" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
						<?endif;?>
					<?endif?>
				<?
				}
				?>
			</div>
			<div class="tlistitem_shadow"></div>
			<?if(!(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) && !$arItem["CAN_BUY"]):?>
        	<div class="badge notavailable"><?=GetMessage("CATALOG_NOT_AVAILABLE2")?></div>
			<?endif?>
		</figure>
<?
	}
endforeach;
?>
    </div>
</div>
<?elseif($USER->IsAdmin()):?>
<h3 class="hitsale"><span></span><?=GetMessage("CR_TITLE_".$arParams["FLAG_PROPERTY_CODE"])?></h3>
<div class="listitem-carousel">
	<?=GetMessage("CR_TITLE_NULL")?>
</div>
<?endif;?>


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<figure class="goods_block"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><span class="img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" width="200" height="200" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>"/></span> <span class="txt"><?=$arItem['NAME']?></span> </a> </figure>
<?endforeach;?>

<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//echo "<pre>";print_r($arResult);echo "</pre>";
$this->setFrameMode(true);
?>
<li>
<?
$i = 0;
if (empty($arResult["ALL_ITEMS"]))
	return;?>
<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>
	<li>
	<label class="menu_section" for="m<?=++$i?>"><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></label>
	<input id="m<?=$i?>" type="checkbox" hidden>
	<?if (is_array($arColumns) && count($arColumns) > 0):?>
		<ul class="mobile_hide">
		<?foreach($arColumns as $key=>$arRow):?>
			<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>
				<li class="<?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>active<?endif?>"><a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a></li>
			<?endforeach;?>
		<?endforeach;?>
		</ul>
	</li>
	<?endif;?>
<?endforeach;?>

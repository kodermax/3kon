<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="top_menu">
	<ul>
	<?
	$count = count($arResult);
	$i = 0;
	foreach($arResult as $arItem):
		$i++;
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
	?><li>
		<?if($arItem["SELECTED"]):?>
		<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		<?else:?>
			<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		<?endif?>
		</li>
	<?endforeach?>
	</ul>
</nav>
<?endif?>
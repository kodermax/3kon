<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CUtil::InitJSCore();
CJSCore::Init(array("jquery"));
?>
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/FurnitureStore">
<head id="Head">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<?
    $APPLICATION->ShowHead();
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/colors.css');
    $APPLICATION->AddHeadScript("/layout/js/dropdown.js");
    ?>
	<title><?$APPLICATION->ShowTitle()?></title>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-64512910-1', 'auto');
ga('send', 'pageview');

</script>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<section class="wrapper">
    <div class="layout">
        <header class="header">
            <table class="header__sections">
                <tr>
                    <td class="header__sections-item m-sections_logo">
                        <div class="header__sections-inner">
                            <a href="/"><img src="/images/logo.png" width="205" height="23" alt="Интернет-магазин мебели" title="Интернет-магазин мебели"></a>
                            <span class="logo__text">интернет-магазин мебели для дома</span>
                        </div>
                    </td>
                    <td class="header__sections-item" style="vertical-align:top;">
                        <div class="header__sections-inner">
                            <div class="header__phone-line">
                                <div>Телефон в Москве:</div><div itemprop="telephone" class="big-phone">(495)&nbsp;972-96-79</div>
                            </div>
                        </div>
                    </td>
                    <td class="header__sections-item" style="vertical-align:top">
                        <div class="header__sections-inner">
                            <nav>
                                <ul class="small-menu">
                                    <li class="small-menu__item">
                                        <a href="/to_buyers/" class="small-menu__link">Как заказать</a>
                                    </li>
                                    <li class="small-menu__item">
                                        <a href="/payment/" class="small-menu__link">Оплата</a>
                                    </li>
                                    <li class="small-menu__item">
                                        <a href="/delivery/" class="small-menu__link">Доставка</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </td>
                </tr>
            </table>
        </header>
        <nav class="main-menu layout">
            <ul class="main-menu__list">
                <li class="main-menu__item">
                    <a href="/catalog/" class="main-menu__link">Каталог мебели</a>
                    <span class="main-menu__arrow dropdown-toggle" data-toggle="dropdown"></span>
                    <div class="dropdown-box m-dropdown-box_catalog">
                        <div class="dropdown-box__inner">
                            <? $APPLICATION->IncludeComponent("mebel:menu.sections", "", array(
                                "IS_SEF" => "Y",
                                "SEF_BASE_URL" => "",
                                "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                                "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                                "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                                "IBLOCK_ID" => 2,
                                "DEPTH_LEVEL" => "1",
                                "CACHE_TYPE" => "N",
                            ), false, Array('HIDE_ICONS' => 'Y'));
                            ?>
                        </div>
                        <div class="dropdown-box__bottom">
                            <a href="/catalog/" class="dropdown-box__link">перейти в каталог</a>
                        </div>
                    </div>
                </li>
                <li class="main-menu__item">
                    <a href="/about/" class="main-menu__link">О магазине</a>
                </li>
                <li class="main-menu__item">
                    <a href="/to_buyers/" class="main-menu__link">Покупателям</a>
                </li>
                <li class="main-menu__item">
                    <a href="/contacts/" class="main-menu__link">Контактная информация</a>
                </li>
                <li class="main-menu__item m-main-menu-item_basket">
                    <span class="main-menu__arrow dropdown-toggle" data-toggle="dropdown"></span>
                    <a href="/personal/cart/" class="main-menu__link">Корзина</a>
                    <div class="dropdown-box m-dropdown-box_basket">
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mebel", array(
                                "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                                "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                "SHOW_PERSONAL_LINK" => "N"
                            ),
                            false,
                            array()
                        );?>
                    </div>
                </li>
            </ul>
        </nav>
        <section class="content">

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
</section>
</div>
<footer class="footer">
    <div class="footer__bg"></div>
    <div class="layout">
        <div class="footer__inner">
            <table class="footer__sections">
                <tr>
                    <td class="footer__sections-item" colspan="2">
                        <div class="footer__logo">
                            <a href="/">
                                <img src="/images/logo_small.png" width="133" height="15" alt="интернет-магазин мебели" title="интернет-магазин мебели">
                            </a>
                        </div>
                        <div class="footer__copy-line" itemprop="name">&copy;&nbsp;Интернет-магазин мебели &laquo;Мебель таргет&raquo;</div>
                        <div class="footer__copy-line">Телефон в Москве: <div itemprop="telephone" class="big-phone">(495)&nbsp;972-96-79</div></div>
                        <!--<ul class="im-list m-im-list_footer">
                            <li class="im-list__item">
                                <span class="im-list__icon m-icon_icq"> </span>
                                <span class="im-list__type">ICQ:</span>
                                <span class="im-list__user-name">555-55-55</span>
                            </li>
                            <li class="im-list__item">
                                <span class="im-list__icon m-icon_skype"> </span>
                                <span class="im-list__type">Skype:</span>
                                <span class="im-list__user-name">mebel-target</span>
                            </li>
                        </ul>-->
                    </td>
                    <td class="footer__sections-item">
                        <nav>


                            <ul class="links-list">
                                <li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/">Главная</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/about/">О магазине</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/to_buyers/">Покупателям</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/to_buyers/">Как заказать</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/payment/">Оплата</a></li><li class='links-list__item'><a class='links-list__link' href="http://www.mebel-target.ru/delivery/">Доставка</a></li>    </ul>

                        </nav>
                    </td>
                    <td class="footer__sections-item" colspan="2">
                            <? $APPLICATION->IncludeComponent("mebel:menu.sections", "footer", array(
                                "IS_SEF" => "Y",
                                "SEF_BASE_URL" => "",
                                "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                                "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                                "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                                "IBLOCK_ID" => 2,
                                "DEPTH_LEVEL" => "1",
                                "CACHE_TYPE" => "N",
                            ), false, Array('HIDE_ICONS' => 'Y'));
                            ?>
                    </td>
                </tr>
            </table>
            <div class="footer__basket">
                <div class="footer__basket-title"><a href="/personal/cart/">Корзина</a></div>
                <div class="footer__basket-data">
                    В корзине пока ничего нет. <a href="/catalog/">Перейти в каталог</a>.
                </div>
            </div>
        </div>
    </div>
</footer>
</section>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter19358860 = new Ya.Metrika({
                    id:19358860,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/19358860" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
<?
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_COMPARE"] = "Сравнить";
$MESS["CT_BCS_TPL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BCS_TPL_MESS_BTN_BUY"] = "Купить";
$MESS["CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "Нет в наличии";
$MESS["CT_BCS_TPL_MESS_BTN_DETAIL"] = "Подробнее";
$MESS["CT_BCS_TPL_MESS_BTN_SUBSCRIBE"] = "Подписаться";
$MESS["CATALOG_SET_BUTTON_BUY"] = "Перейти в корзину";
$MESS["ADD_TO_BASKET_OK"] = "Товар добавлен в корзину";
$MESS["CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE"] = "от #PRICE# за #MEASURE#";
$MESS["CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE"] = "#VALUE# #UNIT#";
$MESS['CR_TITLE_new'] = 'Новые поступления';
$MESS['CP_TITLE_new'] = 'новинка';
$MESS['CR_TITLE_top'] = 'Популярная мебель';
$MESS['CP_TITLE_top'] = 'популярная мебель';
$MESS['CP_LINK_ALSO_top'] = 'показать еще популярные';
$MESS['CP_LINK_ALSO_new'] = 'показать еще новинки';
?>
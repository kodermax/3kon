<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
    <h1><?=GetMessage("CR_TITLE_".$arParams["SECTION_CODE"])?></h1>
<?
if (!empty($arResult['ITEMS']))
{
    ?>
<ul class="catalog">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <li class="catalog__element" itemscope="" itemtype="http://schema.org/Product"><div class="catalog__element-header">
                <div class="catalog__element-inner">
                    <figure>
                        <span class="catalog__element-img-box"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" width="205" height="205" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" itemprop="image"></a></span>
                        <ficaption class="catalog__element-name">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog__element-link" title="<?=$arItem["NAME"]?>" itemprop="name"><?=$arItem["NAME"]?></a>
                            <meta itemprop="url" content="http://www.mebel-target.ru/<?=$arItem["DETAIL_PAGE_URL"]?>">
                        </ficaption>
                    </figure>
                    <div class="catalog___element-desc" itemprop="description"><?=$arItem["PREVIEW_TEXT"]?></div>
                </div>
                <span class="catalog__element-action-text m-catalog-element-action-text_new"><?=GetMessage("CP_TITLE_".$arParams["SECTION_CODE"])?></span>
            </div><div class="catalog__element-footer">
                <div class="catalog__element-inner" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                    <span class="catalog__element-price m-catalog-element-price_new" itemprop="price"><?=$arItem["MIN_PRICE"]["PRINT_VALUE"]?></span>
                </div>
            </div>
        </li>
    <?endforeach;?>
</ul>
    <footer class="content-section-switcher">
        <div class="content-section-switcher__button">
            <a href="/catalog/<?=$arParams['SECTION_CODE']?>/" class="content-section-switcher__link"><?=GetMessage("CP_LINK_ALSO_".$arParams['SECTION_CODE']);?></a>
        </div>
    </footer>
    <?
}
?>
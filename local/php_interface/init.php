<?

define('SITE_ID','s2');
CJSCore::RegisterExt("msdropdown", Array(
    "js"  =>    "/local/assets/js/jquery.dd.min.js",
    "css" =>    "/local/assets/css/msdropdown/dd.css",
    "rel" =>   array('jquery')
));

require_once($_SERVER['DOCUMENT_ROOT']."/local/php_interface/mebel/prop_directory_color.php");
//require_once($_SERVER['DOCUMENT_ROOT']."/local/php_interface/mebel/product_provider.php");
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyDirectoryColor", "GetUserTypeDescription"));
AddEventHandler('sale', 'OnOrderNewSendEmail','OnOrderNewSendEmail');
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAdd");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdate");
AddEventHandler("catalog", "OnPriceAdd", "OnPriceAdd");
AddEventHandler("catalog", "OnPriceUpdate", "OnPriceUpdate");
// обработка If-Modified-Since
$LastModified_unix = strtotime(date("D, d M Y H:i:s", filectime($_SERVER['SCRIPT_FILENAME']))); $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix); $IfModifiedSince = false; if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))    $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5)); if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))    $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5)); if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');    exit; } header('Last-Modified: '. $LastModified);

function updatePricesForCatalog($Id, $arFields){
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    if (CModule::IncludeModule('currency'))
        $strDefaultCurrency = CCurrency::GetBaseCurrency();
    //Get iblock element
    $rsPriceElement = CIBlockElement::GetList(
        array(),
        array(
            "ID" => $arFields["PRODUCT_ID"],
        ),
        false,
        false,
        array("ID", "IBLOCK_ID")
    );
    if($arPriceElement = $rsPriceElement->Fetch())
    {
        $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
        if(is_array($arCatalog))
        {
            //Check if it is offers iblock
            if($arCatalog["OFFERS"] == "Y")
            {
                //Find product element
                $rsElement = CIBlockElement::GetProperty(
                    $arPriceElement["IBLOCK_ID"],
                    $arPriceElement["ID"],
                    "sort",
                    "asc",
                    array("ID" => $arCatalog["SKU_PROPERTY_ID"])
                );
                $arElement = $rsElement->Fetch();
                if($arElement && $arElement["VALUE"] > 0)
                {
                    $ELEMENT_ID = $arElement["VALUE"];
                    $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
                    $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
                }
            }
            //or iblock which has offers
            elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
            {
                $ELEMENT_ID = $arPriceElement["ID"];
                $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
                $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
            }
            //or it's regular catalog
            else
            {
                $ELEMENT_ID = $arPriceElement["ID"];
                $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                $OFFERS_IBLOCK_ID = false;
                $OFFERS_PROPERTY_ID = false;
            }
        }
    }
    if($ELEMENT_ID)
    {
        static $arPropCache = array();
        if(!array_key_exists($IBLOCK_ID, $arPropCache))
        {
            //Check for MINIMAL_PRICE property
            $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();
            if($arProperty)
                $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            else
                $arPropCache[$IBLOCK_ID] = false;
        }

        if($arPropCache[$IBLOCK_ID])
        {
            //Compose elements filter
            if($OFFERS_IBLOCK_ID)
            {
                $rsOffers = CIBlockElement::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                        "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                    ),
                    false,
                    false,
                    array("ID")
                );
                while($arOffer = $rsOffers->Fetch())
                    $arProductID[] = $arOffer["ID"];

                if (!is_array($arProductID))
                    $arProductID = array($ELEMENT_ID);
            }
            else
                $arProductID = array($ELEMENT_ID);

            $minPrice = false;
            $maxPrice = false;
            //Get prices
            $rsPrices = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $arProductID,
                )
            );
            while($arPrice = $rsPrices->Fetch())
            {
                if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
                    $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);

                $PRICE = $arPrice["PRICE"];

                if($minPrice === false || $minPrice > $PRICE)
                    $minPrice = $PRICE;

                if($maxPrice === false || $maxPrice < $PRICE)
                    $maxPrice = $PRICE;
            }

            //Save found minimal price into property
            if($minPrice !== false)
            {
                CIBlockElement::SetPropertyValuesEx(
                    $ELEMENT_ID,
                    $IBLOCK_ID,
                    array(
                        "MINIMUM_PRICE" => $minPrice,
                        "MAXIMUM_PRICE" => $maxPrice,
                    )
                );
            }
        }
    }
}
function updatePricesForIBlock($arFields){
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    if (CModule::IncludeModule('currency'))
        $strDefaultCurrency = CCurrency::GetBaseCurrency();

    $arOffers = CIBlockPriceTools::GetOffersIBlock($arFields["IBLOCK_ID"]);
    if(is_array($arOffers))
    {
        $ELEMENT_ID = $arFields["ID"];
        $IBLOCK_ID = $arFields["IBLOCK_ID"];
        $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
        $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
    }
    if($ELEMENT_ID)
    {
        static $arPropCache = array();
        if(!array_key_exists($IBLOCK_ID, $arPropCache))
        {
            //Check for MINIMAL_PRICE property
            $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();
            if($arProperty)
                $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            else
                $arPropCache[$IBLOCK_ID] = false;
        }

        if($arPropCache[$IBLOCK_ID])
        {
            //Compose elements filter
            if($OFFERS_IBLOCK_ID)
            {
                $rsOffers = CIBlockElement::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                        "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                    ),
                    false,
                    false,
                    array("ID")
                );
                while($arOffer = $rsOffers->Fetch())
                    $arProductID[] = $arOffer["ID"];

                if (!is_array($arProductID))
                    $arProductID = array($ELEMENT_ID);
            }
            else
                $arProductID = array($ELEMENT_ID);

            $minPrice = false;
            $maxPrice = false;
            //Get prices
            $rsPrices = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $arProductID,
                )
            );
            while($arPrice = $rsPrices->Fetch())
            {
                if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
                    $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);

                $PRICE = $arPrice["PRICE"];

                if($minPrice === false || $minPrice > $PRICE)
                    $minPrice = $PRICE;

                if($maxPrice === false || $maxPrice < $PRICE)
                    $maxPrice = $PRICE;
            }

            //Save found minimal price into property
            if($minPrice !== false)
            {
                CIBlockElement::SetPropertyValuesEx(
                    $ELEMENT_ID,
                    $IBLOCK_ID,
                    array(
                        "MINIMUM_PRICE" => $minPrice,
                        "MAXIMUM_PRICE" => $maxPrice,
                    )
                );
            }
        }
    }
}
function OnPriceAdd($Id, $arFields){
    updatePricesForCatalog($Id, $arFields);
}
function OnPriceUpdate($Id, $arFields){
    updatePricesForCatalog($Id, $arFields);
}

function OnAfterIBlockElementUpdate($arFields){
    updatePricesForIBlock($arFields);
}
function OnAfterIBlockElementAdd(&$arFields){
    CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('ARTICLE' => $arFields['ID']));
    updatePricesForIBlock($arFields);
}
//AddEventHandler('sale', 'OnBeforeBasketAdd', 'OnBeforeBasketAdd');
function OnBeforeBasketAdd(&$arFields)
{
    foreach($arFields['PROPS'] as $key => $arProp)
    {
        if($arProp['CODE'] == 'COLOR_REF')
        {
            //сначала выбрать информацию о ней из базы данных
            $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
            $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
            $query = new \Bitrix\Main\Entity\Query($hlentity);
            $query->setSelect(array('UF_NAME'))
                  ->setFilter(array('UF_XML_ID' => $arProp['VALUE']));
            $result = new CDBResult($query->exec());
            if($row = $result->fetch())
            {

                $arFields['PROPS'][$key]['VALUE'] = CUtil::PhpToJSObject(array('XML_ID' => $arProp['VALUE'],'NAME' => $row['UF_NAME']));
            }
        }
        if($arProp['CODE'] == 'SIZE_REF')
        {
            //сначала выбрать информацию о ней из базы данных
            $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(3)->fetch();
            $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
            $query = new \Bitrix\Main\Entity\Query($hlentity);
            $query->setSelect(array('UF_NAME'))
                ->setFilter(array('UF_XML_ID' => $arProp['VALUE']));
            $result = new CDBResult($query->exec());
            if($row = $result->fetch())
            {
                $arFields['PROPS'][$key]['VALUE'] = CUtil::PhpToJSObject(array('XML_ID' => $arProp['VALUE'],'NAME' => $row['UF_NAME']));
            }
        }
    }
    return $arFields;
}
function OnOrderNewSendEmail($ID, $eventName, &$arFields)
{
    \Bitrix\Main\Loader::includeModule('sale');

    $list = CSaleBasket::GetList([],['ORDER_ID' => $ID],false,false,['ID', 'PRODUCT_ID', 'NAME', 'QUANTITY', 'PRICE']);
    $arFields['ORDER_LIST'] = '<table cellspacing="2" cellpadding="2" border="1">
    <thead><tr><th>#</th><th>Наименование товара</th><th>Параметры товара</th><th>Количество</th><th>Цена, руб.</th><th>Сумма, руб.</th></tr></thead>
    <tbody>';
    $i = 1;
    $allSum = 0;
    while($row = $list->GetNext())
    {
        $resProps = CSaleBasket::GetPropsList(
            array(
                "SORT" => "ASC",
                "NAME" => "ASC"
            ),
            array("BASKET_ID" => $row['ID'])
        );
        $color = $size = '';
        while ($arProp = $resProps->Fetch())
        {
            if ($arProp['CODE'] == 'COLOR_REF')
            {
                $color = $arProp['VALUE'];
            }
            if ($arProp['CODE'] == 'SIZE_REF')
            {
                $size = $arProp['VALUE'];
            }
        }
        $arFields['ORDER_LIST'] .= '<tr><td>'.$i++.'</td><td>'.$row['NAME'].'</td><td>'.$size.','. $color.'</td><td>'.$row['QUANTITY'].'</td><td>'.SaleFormatCurrency($row['PRICE'],'RUB').'</td><td>'.SaleFormatCurrency($row['PRICE']*$row['QUANTITY'], 'RUB').'</td></tr>';
        $allSum += $row['PRICE'] * $row['QUANTITY'];
    }
    $arFields['ORDER_LIST'] .= '<tr><td colspan="6"><strong style="margin-right:60px">Итого:</strong><strong>'.SaleFormatCurrency($allSum,'RUB').'</strong></td></tr></tbody></table>';
    $arFields['CUSTOMER_INFO'] = '<table cellspacing="2" cellpadding="2" border="1">
    <tbody>
    <tr>
        <td>ФИО:</td>
        <td>#FIO#</td>
    </tr>
    <tr>
        <td>Телефон:</td>
        <td>#PHONE#</td>
    </tr>
    <tr>
        <td>E-Mail:</td>
        <td>#EMAIL#</td>
    </tr>
    <tr>
        <td>Город:</td>
        <td>#CITY#</td>
    </tr>
    <tr>
        <td>Улица:</td>
        <td>#STREET#</td>
    </tr>
    <tr>
        <td>Дом:</td>
        <td>#HOUSE#</td>
    </tr>
    <tr>
        <td>Подъезд:</td>
        <td>#PODEZD#</td>
    </tr>
     <tr>
        <td>Этаж:</td>
        <td>#FLOOR#</td>
    </tr>
    <tr>
        <td>Квартира:</td>
        <td>#FLAT#</td>
    </tr>
    </tbody>
    </table>';
    $arFields['CUSTOMER_INFO'] = str_replace('#FIO#', $arFields['ORDER_USER'], $arFields['CUSTOMER_INFO']);
    $arFields['CUSTOMER_INFO'] = str_replace('#EMAIL#',"<a href='". $arFields['EMAIL']."'>".$arFields['EMAIL']."</a>", $arFields['CUSTOMER_INFO']);
    $dbPropValues = CSaleOrderPropsValue::GetList(
        array("SORT" => "ASC"),
        array(
            "ORDER_ID" => $ID
        )
    );
    while ($arValue = $dbPropValues->Fetch())
    {
       if($arValue['CODE'] == 'PHONE')
       {
           $arFields['CUSTOMER_INFO'] = str_replace('#PHONE#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
       }
        if($arValue['CODE'] == 'CITY')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#CITY#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
        if($arValue['CODE'] == 'STREET')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#STREET#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
        if($arValue['CODE'] == 'HOUSE')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#HOUSE#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
        if($arValue['CODE'] == 'PODEZD')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#PODEZD#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
        if($arValue['CODE'] == 'FLAT')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#FLAT#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
        if($arValue['CODE'] == 'FLOOR')
        {
            $arFields['CUSTOMER_INFO'] = str_replace('#FLOOR#',$arValue['VALUE'], $arFields['CUSTOMER_INFO']);
        }
    }
    $arOrder = CSaleOrder::GetByID($ID);
    //Способ доставки
    $deliveryId = $arOrder['DELIVERY_ID'];
    $arDelivery = CSaleDelivery::GetByID($deliveryId);
    $arFields['DELIVERY'] = $arDelivery['NAME']. ' '. SaleFormatCurrency($arFields['DELIVERY_PRICE'], 'RUB');
    //Способ оплаты
    /*$payId = $arOrder['PAY_SYSTEM_ID'];
    $arPaySys = CSalePaySystem::GetByID($payId);
    $arFields['CUSTOMER_INFO'] = str_replace('#PAY#',$arPaySys['NAME'], $arFields['CUSTOMER_INFO']);*/
    return true;
}
?>
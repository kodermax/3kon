<?if (!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog"))
return false;

IncludeModuleLangFile(__FILE__);

class CCatalogProductProviderCustom extends CCatalogProductProvider
{
    public static function GetProductData($arParams)
    {
        if (!is_set($arParams, "QUANTITY") || doubleval($arParams["QUANTITY"]) <= 0)
            $arParams["QUANTITY"] = 0;

        if (!is_set($arParams, "RENEWAL") || $arParams["RENEWAL"] != "Y")
            $arParams["RENEWAL"] = "N";

        if (!is_set($arParams, "USER_ID") || intval($arParams["USER_ID"]) <= 0)
            $arParams["USER_ID"] = 0;

        if (!is_set($arParams["SITE_ID"]))
            $arParams["SITE_ID"] = false;

        if (!is_set($arParams["CHECK_QUANTITY"]) || $arParams["CHECK_QUANTITY"] != "N")
            $arParams["CHECK_QUANTITY"] = "Y";

        if (!is_set($arParams["CHECK_PRICE"]) || $arParams["CHECK_PRICE"] != "N")
            $arParams["CHECK_PRICE"] = "Y";

        if (!array_key_exists('CHECK_COUPONS', $arParams) || 'N' != $arParams['CHECK_COUPONS'])
            $arParams['CHECK_COUPONS'] = 'Y';

        $arParams['BASKET_ID'] = (string)(isset($arParams['BASKET_ID']) ? $arParams['BASKET_ID'] : '0');

        $productID = intval($arParams["PRODUCT_ID"]);
        $quantity  = doubleval($arParams["QUANTITY"]);
        $renewal   = (($arParams["RENEWAL"] == "Y") ? "Y" : "N");
        $intUserID = intval($arParams["USER_ID"]);
        if (0 > $intUserID)
            $intUserID = 0;
        $strSiteID = $arParams["SITE_ID"];

        global $USER;
        global $APPLICATION;

        $arResult = array();

        static $arUserCache = array();
        if (0 < $intUserID)
        {
            if (!isset($arUserCache[$intUserID]))
            {
                $by = 'ID';
                $order = 'DESC';
                $rsUsers = CUser::GetList($by, $order, array("ID_EQUAL_EXACT"=>$intUserID),array('FIELDS' => array('ID')));
                if ($arUser = $rsUsers->Fetch())
                {
                    $arUser['ID'] = intval($arUser['ID']);
                    $arUserCache[$arUser['ID']] = CUser::GetUserGroup($arUser['ID']);
                }
                else
                {
                    $intUserID = 0;
                    return $arResult;
                }
            }

            $dbIBlockElement = CIBlockElement::GetList(
                array(),
                array(
                    "ID" => $productID,
                    "ACTIVE" => "Y",
                    "ACTIVE_DATE" => "Y",
                    "CHECK_PERMISSIONS" => "N",
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL')
            );
            if(!($arProduct = $dbIBlockElement->GetNext()))
                return $arResult;
            if ('E' == CIBlock::GetArrayByID($arProduct['IBLOCK_ID'], "RIGHTS_MODE"))
            {
                $arUserRights = CIBlockElementRights::GetUserOperations($productID, $intUserID);
                if (empty($arUserRights))
                {
                    return $arResult;
                }
                elseif (!is_array($arUserRights) || !array_key_exists('element_read', $arUserRights))
                {
                    return $arResult;
                }
            }
            else
            {
                if ('R' > CIBlock::GetPermission($arProduct['IBLOCK_ID'], $intUserID))
                {
                    return $arResult;
                }
            }
        }
        else
        {
            $dbIBlockElement = CIBlockElement::GetList(
                array(),
                array(
                    "ID" => $productID,
                    "ACTIVE" => "Y",
                    "ACTIVE_DATE" => "Y",
                    "CHECK_PERMISSIONS" => "Y",
                    "MIN_PERMISSION" => "R",
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL')
            );
            if(!($arProduct = $dbIBlockElement->GetNext()))
                return $arResult;
        }

        $rsCatalogs = CCatalog::GetList(
            array(),
            array('IBLOCK_ID' => $arProduct["IBLOCK_ID"]),
            false,
            false,
            array(
                'IBLOCK_ID',
                'SUBSCRIPTION'
            )
        );
        if (!($arCatalog = $rsCatalogs->Fetch()))
        {
            return $arResult;
        }
        if ('Y' == $arCatalog["SUBSCRIPTION"])
        {
            $quantity = 1;
        }

        $dblQuantity = 0;
        $boolQuantity = false;

        $rsProducts = CCatalogProduct::GetList(
            array(),
            array('ID' => $productID),
            false,
            false,
            array(
                'ID',
                'CAN_BUY_ZERO',
                'QUANTITY_TRACE',
                'QUANTITY',
                'WEIGHT',
                'WIDTH',
                'HEIGHT',
                'LENGTH',
                'BARCODE_MULTI',
                'TYPE'
            )
        );

        if ($arCatalogProduct = $rsProducts->Fetch())
        {
            $dblQuantity = doubleval($arCatalogProduct["QUANTITY"]);
            $boolQuantity = ('Y' != $arCatalogProduct["CAN_BUY_ZERO"] && 'Y' == $arCatalogProduct["QUANTITY_TRACE"]);

            if ($arParams["CHECK_QUANTITY"] == "Y" && $boolQuantity && 0 >= $dblQuantity)
            {
                $APPLICATION->ThrowException(GetMessage("CATALOG_NO_QUANTITY_PRODUCT", array("#NAME#" => htmlspecialcharsbx($arProduct["~NAME"]))), "CATALOG_NO_QUANTITY_PRODUCT");
                return $arResult;
            }
        }
        else
        {
            $APPLICATION->ThrowException(GetMessage("CATALOG_ERR_NO_PRODUCT"), "CATALOG_NO_QUANTITY_PRODUCT");
            return $arResult;
        }

        if ($arParams["CHECK_PRICE"] == "Y")
        {
            $strBasketProductID = $productID.'_'.$arParams['BASKET_ID'];
            $arCoupons = array();
            if (0 < $intUserID)
            {
                if ('Y' == $arParams['CHECK_COUPONS'])
                    $arCoupons = CCatalogDiscountCoupon::GetCouponsByManage($intUserID);

                CCatalogDiscountSave::SetDiscountUserID($intUserID);
            }
            else
            {
                if ('Y' == $arParams['CHECK_COUPONS'])
                    $arCoupons = CCatalogDiscountCoupon::GetCoupons();
            }


            $boolChangeCoupons = false;
            $arNewCoupons = array();
            if (!empty($arCoupons) && is_array($arCoupons) && !empty(self::$arOneTimeCoupons))
            {
                foreach ($arCoupons as $key => $coupon)
                {
                    if (isset(self::$arOneTimeCoupons[$coupon]))
                    {
                        if (self::$arOneTimeCoupons[$coupon] == $strBasketProductID)
                        {
                            $boolChangeCoupons = true;
                            $arNewCoupons[] = $coupon;
                        }
                        else
                        {
                            unset($arCoupons[$key]);
                        }
                    }
                }
            }
            if ($boolChangeCoupons)
                $arCoupons = $arNewCoupons;

            $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray()), $renewal, array(), (0 < $intUserID ? $strSiteID : false), $arCoupons);

            if (empty($arPrice))
            {
                if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray())))
                {
                    $quantity = $nearestQuantity;
                    $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray()), $renewal, array(), (0 < $intUserID ? $strSiteID : false), $arCoupons);
                }
            }

            if (empty($arPrice))
            {
                if (0 < $intUserID)
                {
                    CCatalogDiscountSave::ClearDiscountUserID();
                }
                return $arResult;
            }

            $boolDiscountVat = ('N' != COption::GetOptionString('catalog', 'discount_vat', 'Y'));

            $currentPrice = $arPrice["PRICE"]["PRICE"];
            $currentDiscount = 0.0;

            $arPrice['PRICE']['ORIG_VAT_INCLUDED'] = $arPrice['PRICE']['VAT_INCLUDED'];

            if ($boolDiscountVat)
            {
                if ('N' == $arPrice['PRICE']['VAT_INCLUDED'])
                {
                    $currentPrice *= (1 + $arPrice['PRICE']['VAT_RATE']);
                    $arPrice['PRICE']['VAT_INCLUDED'] = 'Y';
                }
            }
            else
            {
                if ('Y' == $arPrice['PRICE']['VAT_INCLUDED'])
                {
                    $currentPrice /= (1 + $arPrice['PRICE']['VAT_RATE']);
                    $arPrice['PRICE']['VAT_INCLUDED'] = 'N';
                }
            }

            $arDiscountList = array();

            if (!empty($arPrice["DISCOUNT_LIST"]))
            {
                $dblStartPrice = $currentPrice;

                foreach ($arPrice["DISCOUNT_LIST"] as &$arOneDiscount)
                {
                    switch ($arOneDiscount['VALUE_TYPE'])
                    {
                        case 'F':
                            if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                                $currentDiscount = $arOneDiscount['VALUE'];
                            else
                                $currentDiscount = CCurrencyRates::ConvertCurrency($arOneDiscount["VALUE"], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);
                            $currentPrice = $currentPrice - $currentDiscount;
                            break;
                        case 'P':
                            $currentDiscount = $currentPrice*$arOneDiscount["VALUE"]/100.0;
                            if (0 < $arOneDiscount['MAX_DISCOUNT'])
                            {
                                if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                                    $dblMaxDiscount = $arOneDiscount['MAX_DISCOUNT'];
                                else
                                    $dblMaxDiscount = CCurrencyRates::ConvertCurrency($arOneDiscount['MAX_DISCOUNT'], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);;
                                if ($currentDiscount > $dblMaxDiscount)
                                    $currentDiscount = $dblMaxDiscount;
                            }
                            $currentPrice = $currentPrice - $currentDiscount;
                            break;
                        case 'S':
                            if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                                $currentPrice = $arOneDiscount['VALUE'];
                            else
                                $currentPrice = CCurrencyRates::ConvertCurrency($arOneDiscount['VALUE'], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);
                            break;
                    }

                    $arOneList = array(
                        'ID' => $arOneDiscount['ID'],
                        'NAME' => $arOneDiscount['NAME'],
                        'COUPON' => '',
                        'MODULE_ID' => 'catalog',
                    );

                    if ($arOneDiscount['COUPON'])
                    {
                        $arOneList['COUPON'] = $arOneDiscount['COUPON'];
                        $dbRes = CCatalogDiscountCoupon::GetList(array(), array('COUPON' => $arOneDiscount['COUPON'], 'ONE_TIME' => 'Y'), false, array('nTopCount' => 1), array('ID'));

                        if ($arRes = $dbRes->Fetch())
                        {
                            self::$arOneTimeCoupons[$arOneDiscount['COUPON']] = $strBasketProductID;
                        }
                    }
                    $arDiscountList[] = $arOneList;
                }
                if (isset($arOneDiscount))
                    unset($arOneDiscount);

                $currentDiscount = $dblStartPrice - $currentPrice;
            }

            if (empty($arPrice["PRICE"]["CATALOG_GROUP_NAME"]))
            {
                if (!empty($arPrice["PRICE"]["CATALOG_GROUP_ID"]))
                {
                    $rsCatGroups = CCatalogGroup::GetListEx(array(),array('ID' => $arPrice["PRICE"]["CATALOG_GROUP_ID"]),false,false,array('ID','NAME','NAME_LANG'));
                    if ($arCatGroup = $rsCatGroups->Fetch())
                    {
                        $arPrice["PRICE"]["CATALOG_GROUP_NAME"] = (!empty($arCatGroup['NAME_LANG']) ? $arCatGroup['NAME_LANG'] : $arCatGroup['NAME']);
                    }
                }
            }

            if (!$boolDiscountVat)
            {
                $currentPrice *= (1 + $arPrice['PRICE']['VAT_RATE']);
                $currentDiscount *= (1 + $arPrice['PRICE']['VAT_RATE']);
                $arPrice['PRICE']['VAT_INCLUDED'] = 'Y';
            }
        }
        else
        {
            $rsVAT = CCatalogProduct::GetVATInfo($productID);
            if ($arVAT = $rsVAT->Fetch())
            {
                $vatRate = doubleval($arVAT['RATE'] * 0.01);
            }
            else
            {
                $vatRate = 0.0;
            }
        }

        $arResult = array(
            "CAN_BUY" => "Y",
            "DETAIL_PAGE_URL" => $arProduct['~DETAIL_PAGE_URL'],
            "BARCODE_MULTI" => $arCatalogProduct["BARCODE_MULTI"],
            "WEIGHT" => floatval($arCatalogProduct['WEIGHT']),
            "DIMENSIONS" => serialize(array(
                    "WIDTH" => $arCatalogProduct["WIDTH"],
                    "HEIGHT" => $arCatalogProduct["HEIGHT"],
                    "LENGTH" => $arCatalogProduct["LENGTH"]
                )
            ),
            "TYPE" => ($arCatalogProduct["TYPE"] == CCatalogProduct::TYPE_SET) ? CCatalogProductSet::TYPE_SET : NULL
        );

        if ($arParams["CHECK_QUANTITY"] == "Y")
            $arResult["QUANTITY"] = ($boolQuantity && $dblQuantity < $quantity) ? $dblQuantity : $quantity;
        else
            $arResult["QUANTITY"] = $arParams["QUANTITY"];

        if ($arParams["CHECK_QUANTITY"] == "Y" && $boolQuantity && $dblQuantity < $quantity)
            $APPLICATION->ThrowException(GetMessage("CATALOG_QUANTITY_NOT_ENOGH", array("#NAME#" => htmlspecialcharsbx($arProduct["~NAME"]), "#CATALOG_QUANTITY#" => $arCatalogProduct["QUANTITY"], "#QUANTITY#" => $quantity)), "CATALOG_QUANTITY_NOT_ENOGH");

        if (0 < $intUserID)
        {
            CCatalogDiscountSave::ClearDiscountUserID();
        }

        if ($arParams["CHECK_PRICE"] == "Y")
        {
            $arResult = array_merge($arResult, array(
                "PRODUCT_PRICE_ID" => $arPrice["PRICE"]["ID"],
                "PRICE" => $currentPrice,
                "CURRENCY" => $arPrice["PRICE"]["CURRENCY"],
                "DISCOUNT_PRICE" => $currentDiscount,
                "NOTES" => $arPrice["PRICE"]["CATALOG_GROUP_NAME"],
                "VAT_RATE" => $arPrice["PRICE"]["VAT_RATE"]
            ));

            if (!empty($arPrice["DISCOUNT_LIST"]))
            {
                $arResult['DISCOUNT_LIST'] = $arDiscountList;
            }
        }
        else
        {
            $arResult["VAT_RATE"] = $vatRate;
        }

        return $arResult;
    }

    public static function OrderProduct($arParams)
    {
        if (!is_set($arParams, "RENEWAL") || $arParams["RENEWAL"] != "Y")
            $arParams["RENEWAL"] = "N";

        if (!is_set($arParams, "USER_ID") || IntVal($arParams["USER_ID"]) <= 0)
            $arParams["USER_ID"] = 0;

        if (!is_set($arParams["SITE_ID"]))
            $arParams["SITE_ID"] = false;

        if (!is_set($arParams["CHECK_QUANTITY"]) || $arParams["CHECK_QUANTITY"] != "N")
            $arParams["CHECK_QUANTITY"] = "Y";

        global $USER;
        global $DB;

        $productID = intval($arParams["PRODUCT_ID"]);
        $quantity = doubleval($arParams["QUANTITY"]);
        $renewal = (($arParams["RENEWAL"] == "Y") ? "Y" : "N");
        $strSiteID = $arParams["SITE_ID"];

        $intUserID = intval($arParams["USER_ID"]);
        if (0 > $intUserID)
            $intUserID = 0;

        $arResult = array();

        static $arUserCache = array();
        if (0 < $intUserID)
        {
            if (!isset($arUserCache[$intUserID]))
            {
                $by = 'ID';
                $order = 'DESC';
                $rsUsers = CUser::GetList($by, $order, array("ID_EQUAL_EXACT"=>$intUserID),array('FIELDS' => array('ID')));
                if ($arUser = $rsUsers->Fetch())
                {
                    $arUser['ID'] = intval($arUser['ID']);
                    $arUserCache[$arUser['ID']] = CUser::GetUserGroup($arUser['ID']);
                }
                else
                {
                    $intUserID = 0;
                    return $arResult;
                }
            }

            $dbIBlockElement = CIBlockElement::GetList(
                array(),
                array(
                    "ID" => $productID,
                    "ACTIVE" => "Y",
                    "ACTIVE_DATE" => "Y",
                    "CHECK_PERMISSION" => "N",
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL')
            );
            if(!($arProduct = $dbIBlockElement->GetNext()))
                return $arResult;

            if ('E' == CIBlock::GetArrayByID($arProduct['IBLOCK_ID'], "RIGHTS_MODE"))
            {
                $arUserRights = CIBlockElementRights::GetUserOperations($productID,$intUserID);
                if (empty($arUserRights))
                {
                    return $arResult;
                }
                elseif (!is_array($arUserRights) || !array_key_exists('element_read',$arUserRights))
                {
                    return $arResult;
                }
            }
            else
            {
                if ('R' > CIBlock::GetPermission($arProduct['IBLOCK_ID'], $intUserID))
                {
                    return $arResult;
                }
            }
        }
        else
        {
            $dbIBlockElement = CIBlockElement::GetList(
                array(),
                array(
                    "ID" => $productID,
                    "ACTIVE" => "Y",
                    "ACTIVE_DATE" => "Y",
                    "CHECK_PERMISSIONS" => "Y",
                    "MIN_PERMISSION" => "R",
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL')
            );
            if(!($arProduct = $dbIBlockElement->GetNext()))
                return $arResult;
        }

        $rsProducts = CCatalogProduct::GetList(
            array(),
            array('ID' => $productID),
            false,
            false,
            array(
                'ID',
                'CAN_BUY_ZERO',
                'QUANTITY_TRACE',
                'QUANTITY',
                'WEIGHT',
                'WIDTH',
                'HEIGHT',
                'LENGTH',
                'BARCODE_MULTI',
                'TYPE'
            )
        );

        if ($arCatalogProduct = $rsProducts->Fetch())
        {
            if ($arParams["CHECK_QUANTITY"] == "Y")
            {
                if ('Y' != $arCatalogProduct["CAN_BUY_ZERO"] && 'Y' == $arCatalogProduct["QUANTITY_TRACE"] && 0 >= doubleval($arCatalogProduct["QUANTITY"]))
                    return $arResult;
            }
        }
        else
        {
            return $arResult;
        }

        if (0 < $intUserID)
        {
            $arCoupons = CCatalogDiscountCoupon::GetCouponsByManage($intUserID);
            CCatalogDiscountSave::SetDiscountUserID($intUserID);
        }
        else
        {
            $arCoupons = CCatalogDiscountCoupon::GetCoupons();
        }

        $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray()), $renewal, array(), (0 < $intUserID ? $strSiteID : false), $arCoupons);

        if (empty($arPrice))
        {
            if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray())))
            {
                $quantity = $nearestQuantity;
                $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, (0 < $intUserID ? $arUserCache[$intUserID] : $USER->GetUserGroupArray()), $renewal, array(), (0 < $intUserID ? $strSiteID : false), $arCoupons);
            }
        }

        if (empty($arPrice))
        {
            if (0 < $intUserID)
            {
                CCatalogDiscountSave::ClearDiscountUserID();
            }
            return $arResult;
        }

        $boolDiscountVat = ('N' != COption::GetOptionString('catalog', 'discount_vat', 'Y'));

        $currentPrice = $arPrice["PRICE"]["PRICE"];
        $currentDiscount = 0.0;

        if ($boolDiscountVat)
        {
            if ('N' == $arPrice['PRICE']['VAT_INCLUDED'])
            {
                $currentPrice *= (1 + $arPrice['PRICE']['VAT_RATE']);
                $arPrice['PRICE']['VAT_INCLUDED'] = 'Y';
            }
        }
        else
        {
            if ('Y' == $arPrice['PRICE']['VAT_INCLUDED'])
            {
                $currentPrice /= (1 + $arPrice['PRICE']['VAT_RATE']);
                $arPrice['PRICE']['VAT_INCLUDED'] = 'N';
            }
        }

        $arDiscountList = array();
        $arCouponList = array();

        if (!empty($arPrice["DISCOUNT_LIST"]))
        {
            $dblStartPrice = $currentPrice;

            foreach ($arPrice["DISCOUNT_LIST"] as &$arOneDiscount)
            {
                switch ($arOneDiscount['VALUE_TYPE'])
                {
                    case 'F':
                        if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                            $currentDiscount = $arOneDiscount['VALUE'];
                        else
                            $currentDiscount = CCurrencyRates::ConvertCurrency($arOneDiscount["VALUE"], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);
                        $currentPrice = $currentPrice - $currentDiscount;
                        break;
                    case 'P':
                        $currentDiscount = $currentPrice*$arOneDiscount["VALUE"]/100.0;
                        if (0 < $arOneDiscount['MAX_DISCOUNT'])
                        {
                            if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                                $dblMaxDiscount = $arOneDiscount['MAX_DISCOUNT'];
                            else
                                $dblMaxDiscount = CCurrencyRates::ConvertCurrency($arOneDiscount['MAX_DISCOUNT'], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);;
                            if ($currentDiscount > $dblMaxDiscount)
                                $currentDiscount = $dblMaxDiscount;
                        }
                        $currentPrice = $currentPrice - $currentDiscount;
                        break;
                    case 'S':
                        if ($arOneDiscount['CURRENCY'] == $arPrice["PRICE"]["CURRENCY"])
                            $currentPrice = $arOneDiscount['VALUE'];
                        else
                            $currentPrice = CCurrencyRates::ConvertCurrency($arOneDiscount['VALUE'], $arOneDiscount["CURRENCY"], $arPrice["PRICE"]["CURRENCY"]);
                        break;
                }

                $arOneList = array(
                    'ID' => $arOneDiscount['ID'],
                    'NAME' => $arOneDiscount['NAME'],
                    'COUPON' => '',
                );

                if ($arOneDiscount['COUPON'])
                {
                    $arOneList['COUPON'] = $arOneDiscount['COUPON'];
                    $arCouponList[] = $arOneDiscount['COUPON'];
                }

                $arDiscountList[] = $arOneList;
            }
            if (isset($arOneDiscount))
                unset($arOneDiscount);

            $currentDiscount = $dblStartPrice - $currentPrice;
        }

        if (empty($arPrice["PRICE"]["CATALOG_GROUP_NAME"]))
        {
            if (!empty($arPrice["PRICE"]["CATALOG_GROUP_ID"]))
            {
                $rsCatGroups = CCatalogGroup::GetListEx(array(),array('ID' => $arPrice["PRICE"]["CATALOG_GROUP_ID"]),false,false,array('ID','NAME','NAME_LANG'));
                if ($arCatGroup = $rsCatGroups->Fetch())
                {
                    $arPrice["PRICE"]["CATALOG_GROUP_NAME"] = (!empty($arCatGroup['NAME_LANG']) ? $arCatGroup['NAME_LANG'] : $arCatGroup['NAME']);
                }
            }
        }

        if (!$boolDiscountVat)
        {
            $currentPrice *= (1 + $arPrice['PRICE']['VAT_RATE']);
            $currentDiscount *= (1 + $arPrice['PRICE']['VAT_RATE']);
            $arPrice['PRICE']['VAT_INCLUDED'] = 'Y';
        }


        $arResult = array(
            "PRODUCT_PRICE_ID" => $arPrice["PRICE"]["ID"],
            "PRICE" => $currentPrice,
            "VAT_RATE" => $arPrice['PRICE']['VAT_RATE'],
            "CURRENCY" => $arPrice["PRICE"]["CURRENCY"],
            "WEIGHT" => floatval($arCatalogProduct["WEIGHT"]),
            "DIMENSIONS" => serialize(array(
                    "WIDTH" => $arCatalogProduct["WIDTH"],
                    "HEIGHT" => $arCatalogProduct["HEIGHT"],
                    "LENGTH" => $arCatalogProduct["LENGTH"]
                )
            ),
            "CAN_BUY" => "Y",
            "DETAIL_PAGE_URL" => $arProduct['~DETAIL_PAGE_URL'],
            "NOTES" => $arPrice["PRICE"]["CATALOG_GROUP_NAME"],
            "DISCOUNT_PRICE" => $currentDiscount,
            "TYPE" => ($arCatalogProduct["TYPE"] == CCatalogProduct::TYPE_SET) ? CCatalogProductSet::TYPE_SET : NULL
        );

        if ($arParams["CHECK_QUANTITY"] == "Y")
            $arResult["QUANTITY"] = $quantity;
        else
            $arResult["QUANTITY"] = $arParams["QUANTITY"];

        if (!empty($arPrice["DISCOUNT_LIST"]))
        {
            $arResult["DISCOUNT_VALUE"] = (100*$currentDiscount/($currentDiscount+$currentPrice))."%";
            $arResult["DISCOUNT_NAME"] = "[".$arPrice["DISCOUNT"]["ID"]."] ".$arPrice["DISCOUNT"]["NAME"];
            $arResult['DISCOUNT_LIST'] = $arDiscountList;

            if (!empty($arPrice["DISCOUNT"]["COUPON"]))
            {
                $arResult["DISCOUNT_COUPON"] = $arPrice["DISCOUNT"]["COUPON"];
            }

            if (!empty($arCouponList))
            {
                $mxApply = CCatalogDiscountCoupon::CouponApply($intUserID, $arCouponList);
            }
        }

        if (0 < $intUserID)
        {
            CCatalogDiscountSave::ClearDiscountUserID();
        }
        return $arResult;
    }

}

?>
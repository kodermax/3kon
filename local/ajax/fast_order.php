<?php

define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER, $APPLICATION;
CUtil::JSPostUnescape();
$APPLICATION->RestartBuffer();
Header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    return;
}
$mode = isset($_POST['mode']) ? $_POST['mode'] : '';
if (!isset($mode[0])) {
    die();
}
$offerID = $_POST["offerId"] ? $_POST["offerId"] : '';
$quantity = $_POST["quantity"] ? $_POST["quantity"] : '1';
$fio = $_POST["fio"] ? $_POST["fio"] : '';
$phone = $_POST["phone"] ? $_POST["phone"] : '';
$mail = $_POST["mail"] ? $_POST["mail"] : '';
if (!isset($offerID[0])) {
    echo CUtil::PhpToJSObject(array('ERROR' => 'ID продукта не найден!'));
    die();
}
if (!isset($quantity[0])) {
    echo CUtil::PhpToJSObject(array('ERROR' => 'Не указано количество!'));
    die();
}

\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Sale;
use Bitrix\Catalog;

$fUserID = CSaleBasket::GetBasketUserID(True);
$fUserID = IntVal($fUserID);
if ($mode === 'FAST_ORDER') {
    $basketId = Add2BasketByProductID($offerID, $quantity, $arRewriteFields, $arProductParams);
    $userId = $GLOBALS['USER']->GetID();
    if ($userId <= 0) {
        $email = !empty($mail) ? $mail : 'no-email@email.com';
        $userId = CSaleUser::DoAutoRegisterUser($email, $fio, SITE_ID, $arErrors);
    }
    $arFields = array(
        "LID" => SITE_ID,
        "PERSON_TYPE_ID" => 1,
        "PAYED" => "N",
        "CANCELED" => "N",
        "STATUS_ID" => "N",
        "CURRENCY" => "RUB",
        "USER_ID" => $userId,
        "PAY_SYSTEM_ID" => 1,
        "DELIVERY_ID" => 1,
        "TAX_VALUE" => 0.0,
        "USER_DESCRIPTION" => "Быстрый заказ"
    );
    $sale = new CSaleOrder();
    $ORDER_ID = $sale->Add($arFields);
    $ORDER_ID = IntVal($ORDER_ID);
    CSaleBasket::OrderBasket($ORDER_ID, $_SESSION["SALE_USER_ID"], SITE_ID);
    $arFields = array(
        "ORDER_ID" => $ORDER_ID,
        "ORDER_PROPS_ID" => 3,
        "NAME" => "Телефон",
        "CODE" => "PHONE",
        "VALUE" => $phone
    );
    CSaleOrderPropsValue::Add($arFields);
    $arFields = array(
        "ORDER_ID" => $ORDER_ID,
        "ORDER_PROPS_ID" => 1,
        "NAME" => "Ф.И.О.",
        "CODE" => "FIO",
        "VALUE" => $fio
    );
    CSaleOrderPropsValue::Add($arFields);
    if (strlen($mail) > 0) {
        $arFields = array(
            "ORDER_ID" => $ORDER_ID,
            "ORDER_PROPS_ID" => 2,
            "NAME" => "E-Mail",
            "CODE" => "EMAIL",
            "VALUE" => $mail
        );
        CSaleOrderPropsValue::Add($arFields);
    }
    $APPLICATION->RestartBuffer();
    echo json_encode(['result' => true]);
    die();
}

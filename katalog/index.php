<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Каталог мебели, интернет магазина 3kon.ru");
$APPLICATION->SetPageProperty("keywords", "Каталог мебели, интернет магазина 3kon.ru");
$APPLICATION->SetPageProperty("title", "Каталог мебели, интернет магазина 3kon.ru");
$APPLICATION->SetTitle("Каталог  интернет магазина 3kon.ru");
?>
    <div class="index_content">
        <h1>Каталог</h1> <br>
        <p><a href="/">Компания 3kon</a> предлагает Вашему вниманию широкий выбор предметов интерьера для дома, дачи и офиса. В нашем каталоге Вы найдете огромный выбор мебели различных брендов от известных мировых производителей. Стиль и качество, простота и удобство, надежность и красота - у нас есть что предложить даже самым изысканным покупателям. Быстро и качественно, наши сотрудники помогут Вам определиться с выбором и подобрать подходящую мебель, исходя из Ваших потребностей и приоритетов. Высокий уровень обслуживания и демократичные цены приятно удивят наших клиентов, а система скидок сделает их постоянными. Мы стараемся сделать Ваше приобритение максимально комфортным во всех смыслах этого слова, а положительные отзывы наших покупателей - верный знак того, что нам это удается!<br><br>
            Более семи лет компания 3kon компания является поставщиком мебели от лучших отечественных и зарубежных фабрик на российский рынок. Многолетний опыт работы позволяет нам предложить Вашему вниманию не только отдельные предметы интерьера, но и целые композиции для кухни, гостинной, бара или офиса. Неоспоримым хитом продаж являются <a href="katalog/zhurnalnyie-stolyi.html">стеклянные столы</a> - новое слово в мире мебели. Сочетая красоту, стиль, изящество и безопасность, стеклянный стол станет украшением любого интерьера, придаст оригинальность классической атмосфере или гармонично дополнит современное дизайнерское решение. Подробнее о достоинствах стеклянных столов Вы можете прочитать внизу страницы.<br><br>
        </p>
        <p><span>Также в <a href="katalog.html">каталоге</a> интернет-магазина мебели <a href="undefined/">3kon</a> представлено множество решений для дома, коттеджа или офиса. Наш ассортимент приятно удивит Вас своим разнообразием - всегда в наличии различные по исполнению и материалам <a href="katalog/stoli-obedennie.html">обеденные столы</a>, <a href="katalog/kuxonnyie-ugolki.html">кухонные уголки</a> и собственно <a href="katalog/kuxni.html">кухни</a>, которые по достоинству оценят домохозяйки, представленные варианты <a href="katalog/banketki.html">банкеток</a>, <a href="katalog/veshalki.html">вешалок</a>, <a href="katalog/obuvniczyi.html">обувниц</a> и <a href="katalog/prixozhie.html">прихожих</a> украсят интерьер прихожей и привнесут удобства в повседневную жизнь, а <a href="katalog/krovatki.html">кровати</a>, <a href="katalog/komodyi.html">комоды</a>, <a href="katalog/detskaya-mebel.html">детская мебель</a> и готовые <a href="katalog/spalni.html">спальни</a> обеспечат комфортный отдых и крепкий здоровый сон для всей семьи. Так же для создания удобного и приятного для глаз интерьера интернет-магазин <a href="undefined/">3kon</a> предлагает разнообразные <a href="katalog/stulya.html">стулья</a>, <a href="katalog/knizhnyie-shkafyi.html">книжные шкафы</a>, <a href="katalog/shkafyi-kupe.html">шкафы-купе</a>, <a href="katalog/stolyi-transformeryi.html">столы-трансформеры</a>, <a href="katalog/kompyuternyie-stolyi.html">компьютерные</a> и <a href="katalog/zhurnalnyie-stolyi.html">журнальные столы</a>, <a href="katalog/tv-tumbyi.html">ТВ-тумбы</a>, а так же прочие <a href="katalog/predmetyi-interera.html">предметы интерьера</a>, которые позволяют создать уникальный дизайн на любой, даже самый изысканный, вкус или дополнить, обновить интерьер, привнеся в него разнообразия. Тем же, кто желает благоустроить свой загородный дом, коттедж или приусадебный участок понравятся предложения представленные в категориях <a href="katalog/dachnaya-mebel(rotang).html">"Плетёная мебель из ротанга"</a> и <a href="katalog/dachnaya-mebel.html">"Дачная мебель"</a> - здесь можно найти весьма изящные решения, отлично гармонирующие с окружающей обстановкой - будь то садовая аллея или дикорастущий парковый сквер. Если же вам надо обновить интерьер офиса, в каталоге интернет-магазина <a href="undefined/">3kon</a> всегда в наличии <a href="katalog/kompyuternyie-stolyi.html">компьютерные столы</a>, <a href="katalog/ofisnyie-kresla.html">офисные кресла</a> и <a href="katalog/ofisnaya-mebel.html">офисная мебель</a> различных моделей и цветовых решений, способных добавить комфорта и повысить эргономику рабочего кабинета или конференц-зала. Вы в любой момент можете заказать любую понравившуюся модель.</span></p>
        <p><span>&nbsp;</span></p>
        <p><span>Мы ценим время наших покупателей и делаем все возможное, чтобы оправдать Ваше доверие.</span></p>
        <div><span><br></span></div>
        <p>&nbsp;</p>
        <p><span style="color: #ff8000;"><strong>(495) 796-41-74, </strong></span><span style="color: #ff8000;"><strong> (в будние дни и субботу с 10.00 до 20.00), пишите на e-mail:&nbsp;&nbsp; <a href="mailto:info@3kon.ru">info@3kon.ru</a> </p>
        <p><strong>наш менеджер с удовольствием ответит на все ваши вопросы.</strong></p>
    </div>
	<table width="100%">


		<tbody><tr><td></td><td></td><td align="center" height="50" valign="middle"><h1>Столы</h1></td><td></td><td></td></tr>
		<tr>
			<td width="20%" height="70" align="center"><a href="/katalog/stoli-obedennie.html"><img height="150" src="/img/katalog/stol_obed.jpg"><br>Столы обеденные</a>
			</td>
			<td width="20%" height="70" align="center"><a href="/katalog/zhurnalnyie-stolyi.html"><img height="150" src="/img/katalog/stol_zhurnal.jpg"><br>Столы журнальные</a>
			</td>
			<td width="20%" height="70" align="center"><a href="/katalog/stolyi-transformeryi.html"><img height="150" src="/img/katalog/stol_transform.jpg"><br>Столы - трансформеры</a>
			</td>
			<td width="20%" height="70" align="center"><a href="/katalog/kompyuternyie-stolyi.html"><img height="150" src="/img/katalog/stol_komp.jpg"><br>Компьютерные столы</a>
			</td>
			<td width="20%" height="70" align="center"><a href="/katalog/gotovyie-resheniya.html"><img height="150" src="/img/katalog/obed_gruppa.jpg"><br>Готовые решения</a>
			</td>
		</tr>
		</tbody></table>
<br>
<br>
<br>
	<table width="100%">
		<tbody><tr><td align="center" height="50" valign="middle"><h1>На кухню</h1></td></tr>
		<tr><td>
				<table width="100%">
					<tbody><tr>
						<td width="25%" height="70" align="center"><a href="/katalog/stulya.html"><img height="150" src="/img/katalog/stul.jpg"><br>Стулья</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/barnaya-mebel.html"><img height="150" src="/img/katalog/bar.jpg"><br>Барная мебель</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/kuxni.html"><img height="150" src="/img/katalog/kuhnya.jpg"><br>Кухни</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/kuxonnyie-ugolki.html"><img height="150" src="/img/katalog/ugolok.jpg"><br>Кухонные уголки</a>
						</td>
					</tr>
					</tbody></table>
			</td></tr>
		</tbody></table>
<br>
<br>
<br>
	<table width="100%">
		<tbody><tr><td align="center" height="50" valign="middle"><h1>В прихожую</h1></td></tr>
		<tr><td>
				<table width="100%">
					<tbody><tr>
						<td width="25%" height="70" align="center"><a href="/katalog/banketki.html"><img height="150" src="/img/katalog/banketka.jpg"><br>Банкетки</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/veshalki.html"><img height="150" src="/img/katalog/veshalka.jpg"><br>Вешалки</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/obuvniczyi.html"><img height="150" src="/img/katalog/obuv.jpg"><br>Обувницы</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/prixozhie.html"><img height="150" src="/img/katalog/prihozhaya.jpg"><br>Прихожие</a>
						</td>
					</tr>
					</tbody></table>
			</td></tr>
		</tbody></table>
<br>
<br>
<br>
	<table width="100%">
		<tbody><tr>
			<td width="50%" align="center" height="50" valign="middle"><h1>Мебель для офиса</h1></td>
			<td width="50%" align="center" height="50" valign="middle"><h1>Шкафы</h1></td>
		</tr>
		<tr><td>
				<table width="100%">
					<tbody><tr>
						<td width="25%" height="70" align="center"><a href="/katalog/ofisnyie-kresla.html"><img height="150" src="/img/katalog/ofis_kreslo.jpg"><br>Офисные кресла</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/ofisnaya-mebel.html"><img height="150" src="/img/katalog/ofis_mebel.jpg"><br>Офисная мебель</a>
						</td>
					</tr>
					</tbody></table>
			</td>
			<td>
				<table width="100%">
					<tbody><tr>
						<td width="25%" height="70" align="center"><a href="/katalog/knizhnyie-shkafyi.html"><img height="150" src="/img/katalog/knigoshkaf.jpg"><br>Книжные шкафы</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/shkafyi-kupe.html"><img height="150" src="/img/katalog/kupe.jpg"><br>Шкафы-купе</a>
						</td>
					</tr>
					</tbody></table>
			</td></tr>
		</tbody></table>
<br>
<br>
<br>
	<table width="100%">
		<tbody><tr><td align="center" height="50" valign="middle"><h1>Для спальни</h1></td></tr>
		<tr><td>
				<table width="100%">
					<tbody><tr>
						<td width="25%" height="70" align="center"><a href="/katalog/spalni.html"><img height="150" src="/img/katalog/spalnya.jpg"><br>Спальни</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/detskaya-mebel.html"><img height="150" src="/img/katalog/detskaya.png"><br>Детская мебель</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/krovatki.html"><img height="150" src="/img/katalog/krovat.jpg"><br>Кровати</a>
						</td>
						<td width="25%" height="70" align="center"><a href="/katalog/komodyi.html"><img height="150" src="/img/katalog/komod.png"><br>Комоды</a>
						</td>
					</tr>
					</tbody></table>
			</td></tr>
		</tbody></table>
<br>
<br>
<br>
	<table width="100%">
		<tbody><tr>
			<td width="33%" align="center" height="50" valign="middle"><h1>Для дачи</h1></td>
			<td width="67%" align="center" height="50" valign="middle"><h1>Разное</h1></td>
		</tr>
		<tr><td>
				<table width="100%">
					<tbody><tr>
						<td width="100" height="70" align="center"><a href="/katalog/dachnaya-mebel(rotang).html"><img height="150" src="/img/katalog/rotang.jpg"><br>Плетёная мебель из ротанга</a>
						</td>

					</tr>
					</tbody></table>
			</td>
			<td>
				<table width="100%">
					<tbody><tr>
						<td width="50%" height="70" align="center"><a href="/katalog/parketyi.html"><img height="150" src="/img/katalog/ucenka.jpg"><br>Уцененный товар</a>
						</td>
						<td width="50%" height="70" align="center"><a href="/katalog/tv-tumbyi.html"><img height="150" src="/img/katalog/TV.jpg"><br>ТВ -тумбы</a>
						</td>
					</tr>
					</tbody></table>
			</td></tr>
		</tbody></table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
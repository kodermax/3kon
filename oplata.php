<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Способы оплаты в интернет магазине 3kon.ru");
$APPLICATION->SetPageProperty("keywords", "Способы оплаты в интернет магазине 3kon.ru");
$APPLICATION->SetPageProperty("description", "Способы оплаты в интернет магазине 3kon.ru");
$APPLICATION->SetTitle("Оплата");
?><h3><br>
 </h3>
<h3><br>
 </h3>
<h3><span style="color: #f7941d; font-size: 18pt;">Оплата наличными</span></h3>
<div>
 <br>
</div>
<p align="justify">
 <span style="font-size: 13pt;">Оплата производится курьеру-экспедитору&nbsp;3kon.ru&nbsp;у Вас на дому или в офисе по предварительному&nbsp;согласованию*. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;">
	Вы получаете на руки договор и квитанцию об оплате. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;"> <span style="color: #003562;"><b>*</b><span style="color: #000000;"> <b><span style="color: #005b7e;">по некоторым позициям мебели требуется предоплата</span></b> (заказная мягкая мебель, заказные кухонные гарнитуры, заказная корпусная мебель). Оставшиеся сумму заказа Вы можете внести наличными в срок, указанный в договоре, двумя способами – оплата в офисе&nbsp;3kon.ru, оплата курьеру-экспедитору.</span></span></span><span style="color: #00aeef;"> </span>
</p>
 <span style="color: #00aeef;"> </span><br>
<h3><span style="color: #f7941d; font-size: 18pt;">Безналичный расчет&nbsp;*</span></h3>
 <span style="color: #f7941d; font-size: 18pt;"><br>
 </span>
<p align="justify">
 <span style="font-size: 13pt;">Оплата заказа мебели через интернет-магазин&nbsp;3kon.ru&nbsp;производится по выставленному счету. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;">
	При приемке сформированного заказа на месте представителю организации необходимо иметь при себе паспорт либо доверенность организации, на которую оформлен заказ. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;">
	* <span style="color: #005b7e;"><b>Внимание!</b></span> Во избежание недоразумений, перед оплатой мебели свяжитесь с нами по почте или по телефону для обсуждения возможности оплаты мебели безналичной формой оплаты.</span>
</p>
 <br>
 <span style="font-size: 18pt;"> </span>
<h3><span style="color: #f7941d; font-size: 18pt;">Оплата электронными деньгами</span><span style="color: #f7941d;">*</span></h3>
 <br>
<h3><span style="color: #0076a4; font-size: 14pt;">Яндекс-деньги&nbsp;*</span></h3>
 <span style="color: #0076a4; font-size: 14pt;"><br>
 </span>
<p align="justify">
 <span style="font-size: 13pt;">Владельцы электронного кошелька Яндекс-Деньги также могут легко и быстро совершить оплату заказа мебели от&nbsp;3kon.ru&nbsp;&nbsp;на номер:&nbsp;4100136817484&nbsp;через свой личный аккаунт. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;">
	Кроме того, Вы можете перевести наличные средства в салонах связи Евросеть. При зачислении платежа в Евросети комиссия не взимается. </span>
</p>
 <span style="font-size: 13pt;"> </span>
<p align="justify">
 <span style="font-size: 13pt;">
	*<b><span style="color: #005b7e;"> Внимание! </span></b>Во избежание недоразумений, перед оплатой мебели свяжитесь с нами по почте или по телефону для обсуждения возможности оплаты мебели электронными деньгами.</span>
</p>
<p align="justify">
	<br>
</p>
<p align="justify">
</p>
<p align="justify">
 <span style="font-size: 13pt;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><a href="https://www.3kon.ru/dostavka.html"><b><span style="color: #f7941d; font-size: 14pt;">далее: доставка мебели&nbsp;&gt;&gt;&gt;</span></b></a>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
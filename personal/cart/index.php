<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Здесь вы можете посмотреть заказанные вами товары  интернет магазине 3kon.ru.");
$APPLICATION->SetPageProperty("keywords", "Корзина интернет магазина 3kon.ru,купить интернет магазина 3kon.ru");
$APPLICATION->SetPageProperty("title", "Корзина интернет магазина 3kon.ru");
$APPLICATION->SetTitle("Корзина");
?><nav><?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"basket", 
	array(
		"ACTION_VARIABLE" => "action",
		"COLUMNS_LIST" => array(
			0 => "PRICE",
			1 => "QUANTITY",
			2 => "DELETE"
		),
		"COMPONENT_TEMPLATE" => "basket",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "N",
		"OFFERS_PROPS" => array(
		),
		"PATH_TO_ORDER" => "/korzina/oformlenie-pokupki.html",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"USE_GIFTS" => "N",
		"USE_PREPAYMENT" => "N"
	),
	false
);?><br>
 </nav><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
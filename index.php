<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Интернет-магазин мебели - банкетки, обувницы");
$APPLICATION->SetPageProperty("keywords", "банкетки, столы трансформеры, обувницы, столы с плиткой, стулья, столы журнальные, офисные кресла");
$APPLICATION->SetPageProperty("description", "3kon.ru предлагает широкий ассортимент банкеток. В каталоге представлены столы с плиткой, стулья, обувницы, столы журнальные, офисные кресла.");
$APPLICATION->SetTitle("Интернет-магазин мебели - банкетки, обувницы, столы с плиткой, офисные кресла, кухонные столы");
$arrFilter = array('PROPERTY_NEW_VALUE' => 'Y');
?><section class="goods padding_top">
<h2 class="red h3">НОВИНКИ</h2>
 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.top",
	"new",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "20",
		"ELEMENT_SORT_FIELD" => "SORT",
		"ELEMENT_SORT_FIELD2" => "timestamp_x",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_CART_PROPERTIES" => array(),
		"OFFERS_FIELD_CODE" => array("",""),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array("",""),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PROPERTY_CODE" => array("",""),
		"ROTATE_TIMER" => "30",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PAGINATION" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "site",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "BANNER"
	)
);?> </section>
<? ob_start(); ?>
<h1 class="h2">Немного о стеклянных столах</h1>
<p>
	 Несмотря на то, что в России <strong>стеклянные столы</strong> появились относительно недавно, они уже успели завоевать большую популярность среди покупателей, чему способствовало во многом их практичность. Ведь в конструкции этих предметов мебели достигнута превосходная гармония высокой эстетики, надёжности и комфорта.<br>
	 Отличная внешняя сочетаемость стекла с различными материалами: металлом, ротангом, пластиком или деревом позволяет изготавливать каркасы изделий практически из чего угодно, а это в свою очередь делает возможным просто и быстро подобрать стол под любой стиль интерьера, будь то классика, или хай тек. В частности, журнальный <strong>стеклянный стол</strong> станет отличным дополнением интерьера любой гостиницы и спальни в независимости от их оформления. А благодаря внешней лёгкости стеклянные столы отлично вписываются во внутреннее убранство небольшой кухни или комнаты, совершенно не загромождая ценное пространство.<br>
	 Стеклянные столы не знают что такое грязь и царапины. С помощью тряпки такой журнальный, обеденный или <strong>компьютерный стол</strong> легко очищается от разлитого масла, клея, пепла, детских фломастеров, остатков пищевых продуктов и других загрязнений, устойчивы они и к воздействию моющих средств. Несмотря на невесомый и хрупкий внешний вид, стеклянные столы обладают высокой прочностью. Разбить крепкое закалённое стекло не так просто как кажется на первый взгляд. Однако, если по какой-то причине столешница всё же подвергнется очень сильному удару, она разлетится на безопасные осколки, которыми невозможно повредить руки. Кроме того, используемое в конструкции стекло защищено специальным слоем, который заметно повышает устойчивость изделия к механическим повреждениям. Даже при интенсивной эксплуатации, например, использования стеклянного стола для приготовления пищи на кухне, столешница всегда будет иметь идеально гладкую поверхность и сверкать как новая долгое время. Безопасен стол и с точки зрения экологии. Применяемые для его изготовления натуральные материалы никогда не вызовут проблем со здоровьем.<br>
	 Наш <b>Интернет-магазин мебели</b> предлагает стеклянные столы разных типов: прямоугольные, круглые или овальные, а в зависимости от типа станины можно выбрать раздвижной, раскладной, стационарный либо стол-трансформер. Мы находимся в г. Москва
</p>
<?
$content = ob_get_contents();
ob_end_clean(); ?>
<? $APPLICATION->AddViewContent('article', $content); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты  интернет магазин 3kon.ru");
$APPLICATION->SetPageProperty("keywords", "Контакты  интернет магазин 3kon.ru");
$APPLICATION->SetPageProperty("description", "Контакты  интернет магазин 3kon.ru");
$APPLICATION->SetTitle("Контакты");
?><nav class="menu_content"><a href="/">Главная</a> » Контакты</nav> <section class="address">
<h1>Контакты</h1>
<dl>
	<dt>Адрес:</dt>
	<dd>Адрес склада: <br>
	 г. Москва, Складочная, 1 стр.8 </dd>
</dl>
<dl>
	<dt>Телефон:</dt>
	<dd><a class="phone_num" href="tel:4957964174">(495) 796-41-74</a>&nbsp;</dd>
</dl>
<dl>
	<dt>E-mail:</dt>
	<dd><a class="mail" href="mailto:info@3kon.ru">info@3kon.ru</a></dd>
</dl>
 <!-- end .content_address -->
<p class="info_text">
	 Мы работаем для Вас с Понедельника по Воскресенье с 09:00 до 20:00
</p>
<p>
 <br>
</p>
<p class="info_text">
	 Посмотреть как добраться с улицы <a target="_blank" href="/contacts/kak-dobratsya.php">Двинцев</a>
</p>
 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array("ZOOM","MINIMAP","TYPECONTROL","SCALELINE"),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.80297576834737;s:10:\"yandex_lon\";d:37.59606249510143;s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.594708463472664;s:3:\"LAT\";d:55.803379610665424;s:4:\"TEXT\";s:56:\"Склад Интернет магазина www.3kon.ru\";}}}",
		"MAP_HEIGHT" => "500",
		"MAP_ID" => "",
		"MAP_WIDTH" => "650",
		"OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING")
	)
);?><br>
<h2>Продавец</h2>
<p>
	 ИП Васильев Александр Сергеевич <br>
	 ОГРНИП: 773371368954
</p>
 <!-- end .address --></section><!-- end .address --><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
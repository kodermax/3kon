<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как проехать");
?>

    <div class="content">
        <h1>Как проехать</h1>
        <div style="font-size: 15px; font-family: Arial,Tahoma,sans; line-height: 1.3;">

            <table border="0" width="16" height="30">
                <tbody>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <div style="font-size: 15px; font-family: Arial,Tahoma,sans; line-height: 1.3;">
                <table border="0">
                    <tbody>
                    <tr>
                        <td><strong style="color: #333333;">Адрес:</strong></td>
                        <td style="padding-left: 18px; padding-bottom: 13px;">
                            <p>Адрес склада:</p>
                            <p>г. Москва, Складочная, 1 стр.9</p>
                        </td>
                    </tr>
                    <tr>
                        <td><strong style="color: #333333;">Телефон:</strong></td>
                        <td style="padding-left: 18px; padding-bottom: 13px;">
                            <p>(495)&nbsp;778–21–71</p>
                            <p>(495) 972-81-82</p>
                        </td>
                    </tr>
                    <tr>
                        <td><strong style="color: #333333;">E-mail:</strong></td>
                        <td style="padding-left: 18px; padding-bottom: 13px;"><a title="Написать письмо" href="mailto:info@3kon.ru">info@3kon.ru</a></td>
                    </tr>
                    <tr>
                        <td><strong style="color: #333333;">ICQ-консультант:</strong></td>
                        <td style="padding-left: 18px; padding-bottom: 10px;">632–761–349</td>
                    </tr>
                    </tbody>
                </table>
                <p style="border-left: 3px solid #ff9900; padding: 10px; margin: 5px 0pt;">Мы работаем для Вас с Понедельника по Пятницу с 10:00 до20:00</p>
                <h1>&nbsp; &nbsp; &nbsp; &nbsp;Схема проезда (с внешней стороны ТТК)</h1>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <ul>
                    <li>Двигаемся по внешней стороне ТТК. Проезжаем мимо тоннеля.</li>
                </ul>
                <p align="left"><a href="http://www.rx300.ru/db.img/DSC_0050_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0050_izmen.razmer_1.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/DSC_0052_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0052_izmen.razmer_1.jpg" alt=""></a></p>
                <p align="left"><a href="http://www.rx300.ru/db.img/DSC_0058_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0058_izmen.razmer_1.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/DSC_0063_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0063_izmen.razmer_1.jpg" alt=""></a></p>
                <ul>
                    <li>
                        <div>Видим справа&nbsp;дом. "Сущевски вал д.&nbsp;23",&nbsp;"ОТП Банк", "ПОЧТА РОССИИ", "МосЕвро Банк".</div>
                    </li>
                </ul>
                <p align="left"><a href="http://www.rx300.ru/db.img/DSC_0160_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0160_izmen.razmer_1.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/DSC_0161_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0161_izmen.razmer_1.jpg" alt=""></a></p>
                <p align="left"><a href="http://www.rx300.ru/db.img/DSC_0163_izmen.razmer.JPG" target="_new"><img src="http://www.rx300.ru/db.img/dsc_0163_izmen.razmer_1.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/dsc_0114_resize.jpg"><img src="http://www.rx300.ru/db.img/dsc_0114_resize_1.jpg" alt=""></a></p>
                <p align="left">&nbsp;</p>
                <ul>
                    <li>Доезжаем до поворота на улицу Двинцев и поворачиваем на нее (проезжаем указатель "ул. Двинцев, Стрелецкая ул." - на фото за столбом).</li>
                </ul>
                <p align="left"><a href="http://www.rx300.ru/db.img/_dsc_0067_3.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0067_3_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0071_4.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0071_4_izmen.razmer.jpg" alt=""></a></p>
                <p align="left">&nbsp;</p>
                <ul>
                    <li>Проезжаем строящееся здание.</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0072_5.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0072_5_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0074_6.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0074_6_izmen.razmer.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Едем прямо по указателю.</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0075_7.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0075_7_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0076_8.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0076_8_izmen.razmer.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Доезжаем до железнодорожного переезда и, пересекая его, едем прямо.</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0077_9.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0077_9_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0079_10.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0079_10_izmen.razmer.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Слева по борту голубое здание – компьютерный супермаркет «Санрайз».</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0081_11.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0081_11_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0083_12.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0083_12_izmen.razmer.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Проезжаем офисный центр «Санрайз».</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0086_13.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0086_13_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/_dsc_0087_14.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0087_14_izmen.razmer.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Доезжаем до угла здания (поворота).</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0088_15.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0088_15_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/dsc_0089_resize.jpg"><img src="http://www.rx300.ru/db.img/dsc_0089_resize_1.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Сворачиваем налево (слева на крыше здания - вывеска)..</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0090_16.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0090_16_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/dsc_0091_resize.jpg"><img src="http://www.rx300.ru/db.img/dsc_0091_resize_1.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>Теперь направо.</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0092_17.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0092_17_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="http://www.rx300.ru/db.img/dsc_0092_resize.jpg"><img src="http://www.rx300.ru/db.img/dsc_0092_resize_1.jpg" alt=""></a></p>
                <p>&nbsp;</p>
                <ul>
                    <li>У шлагбаума сворачиваем налево - мы на месте.</li>
                </ul>
                <p><a href="http://www.rx300.ru/db.img/_dsc_0093_18.jpg" target="_new"><img src="http://www.rx300.ru/db.img/_dsc_0093_18_izmen.razmer.jpg" alt=""></a>&nbsp;&nbsp;&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>

    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
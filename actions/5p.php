<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акция «Заполни анкету и получи скидку 5%»");
?>

    <div class="index_content"><div style="font-size: 15px; font-family: Arial, Tahoma, sans; line-height: 1.3; color: #000; max-width: 750px;">
            <h1 style="font-size: 130%; margin-bottom: 15px;">Акция «Заполни анкету и получи скидку 5%»</h1>
            <p style="color: #000; margin: 15px 0; border-left: 3px solid #ff9900; padding: 10px;">
                <strong>Оформили заказ по телефону</strong>? Заполните анкету и получите скидку! <a title="Получить скидку 5%" rel="nofollow" href="http://bit.ly/o94HQl" target="_blank">Анкета</a>.
            </p>
            <p style="color: #000;">Уважаемые покупатели, предлагаем вам принять участие в анкетировании, с целью повышения качества обслуживания.</p>
            <p style="color: #000;">Обращаем ваше внимание, что анкета подготовлена с использование сервиса "Google документы" компании Google.</p>
            <h2 style="font-size: 110%; margin: 15px 0;">Условия акции</h2>
            <p style="color: #000;">Любой покупатель оформивший заказ может получить <strong>скидку 5% на следующий заказ</strong>.</p>
            <p style="color: #000;">Для получения скидки необходимо ответить на несколько вопросов анкеты. Ссылка на анкету будет доступна после оформления заказ.</p>
            <h2 style="font-size: 110%; margin: 15px 0;">Кто может принять участие в акции</h2>
            <p style="color: #000;">В акции может принять участие любой покупатель оформивший заказ.</p>
            <h2 style="font-size: 110%; margin: 15px 0;">Важная информация</h2>
            <p style="color: #000;">Вашему заказу будет присвоен уникальный номер, его необходимо указать в анкете.</p>
            <p style="color: #000;">Если у вас возникли трудности при заполнении анкеты сообщите нам об этом на <a href="mailto:info@3kon.ru">info@3kon.ru</a></p>
            <h2 style="font-size: 110%; margin: 15px 0;">Конфиденциальность</h2>
            <p style="color: #000;">Все данные передаются по защищенному каналу связи и не будут использованы для рассылки спам сообщений.</p>
            <h2 style="font-size: 110%; margin: 15px 0; border-left: 3px solid #ff9900; padding: 10px;">Приступайте к покупкам и получите скидку 5%!</h2>
        </div></div>
<?
$arrFilter = array('PROPERTY_NEW_VALUE' => 'Y');
$APPLICATION->IncludeComponent("bitrix:catalog.top", "new", Array(
    "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
    "IBLOCK_ID" => "4",	// Инфоблок
    "ELEMENT_SORT_FIELD" => "shows",	// По какому полю сортируем элементы
    "ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
    "ELEMENT_SORT_FIELD2" => "shows",	// Поле для второй сортировки элементов
    "ELEMENT_SORT_ORDER2" => "asc",	// Порядок второй сортировки элементов
    "FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
    "HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
    "ELEMENT_COUNT" => "20",	// Количество выводимых элементов
    "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
    "PROPERTY_CODE" => array(	// Свойства
        0 => "",
        1 => "",
    ),
    "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
    "VIEW_MODE" => "BANNER",
    "SHOW_DISCOUNT_PERCENT" => "N",
    "SHOW_OLD_PRICE" => "N",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Нет в наличии",
    "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
    "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
    "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
    "DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
    "PRICE_CODE" => "",	// Тип цены
    "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
    "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
    "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
    "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
    "BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
    "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
    "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
    "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
    "ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
    "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
    "PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
    "PRODUCT_PROPERTIES" => "",	// Характеристики товара
    "TEMPLATE_THEME" => "site",
    "ADD_PICT_PROP" => "-",
    "LABEL_PROP" => "-",
    "ROTATE_TIMER" => "30",
    "SHOW_PAGINATION" => "Y"
),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>